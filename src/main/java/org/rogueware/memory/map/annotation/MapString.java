/*
 * MemoryMapString.java
 *
 * Defines a class used to annotate a memory mapped string field
 * Currently only supports ASCII
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.rogueware.memory.map.enums.StringType;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MapString {

   String charset() default "US-ASCII";

   StringType type() default StringType.NULL_TERMINATED;

   String stringSizeMember() default "";            // Get the size of the string from a member var

   int stringSize() default 0;             // Use defined size for string

   String terminator() default "\n";          // String terminated string

   boolean swapAlternatingCharacters() default false; // true to swap alternating characters

   int position();
}
