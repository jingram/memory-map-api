/*
 * MemoryMapCallbackInterface.java
 *
 * Defines a class used to define the interface for a callback for a specific structure type (class) when mapped
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public interface MemoryMapCallbackInterface<T> {

   /**
    * Callback when the specific structure of type T is succesfully decoded from
    * a message contained in a byte stream.
    *
    * @param structure Instance of the structure that was decoded
    * @param memoryMap Instance of the memory map class that did the decoding
    */
   public void decoded(T structure, MemoryMap memoryMap);
}
