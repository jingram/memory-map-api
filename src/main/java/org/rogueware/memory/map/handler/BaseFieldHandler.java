/*
 * BaseFieldHandler.java
 *
 * Defines a class used to define the base functions of all field handlers
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map.handler;

import java.lang.reflect.Field;
import org.rogueware.memory.map.enums.ByteOrder;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public abstract class BaseFieldHandler implements FieldHandlerInterface {

   protected ByteOrder defaultByteOrder;
   protected Field field;
   protected String fieldType;
   protected Object structure;

   /**
    * Creates a new instance of BaseFieldHandler
    */
   public BaseFieldHandler() {
   }

   /**
    * Set the field value for introspection purposes
    *
    * @param field The field
    */
   @Override
   public void setField(Field field) {
      this.field = field;

      // Get the field type
      if (field.getType().isArray()) {
         fieldType = field.getType().getComponentType().getName();
      } else {
         fieldType = field.getType().getName();
      }
   }

   /**
    * Set the default byte order to use
    *
    * @param def Default byte order
    */
   @Override
   public void setDefaulByteOrder(ByteOrder def) {
      defaultByteOrder = def;
   }

   /**
    * Set the instance containing the field values and definitions
    *
    * @param structure Instance of the structure
    */
   @Override
   public void setStructureInstance(Object structure) {
      this.structure = structure;
   }

}
