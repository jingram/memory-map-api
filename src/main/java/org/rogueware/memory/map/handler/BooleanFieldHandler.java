/*
 * BooleanFieldHandler.java
 *
 * Defines a class used to handle reading and writing boolean fields
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map.handler;

import java.lang.annotation.Annotation;
import java.nio.ByteBuffer;
import org.rogueware.memory.map.MemoryMapException;
import org.rogueware.memory.map.annotation.MapBoolean;
import org.rogueware.memory.map.enums.BooleanBit;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class BooleanFieldHandler extends BaseFieldHandler {

   private BooleanBit bit = BooleanBit.BYTE;
   private boolean incrementOffset = true;

   private int bitNum;

   /**
    * Creates a new instance of MapBooleanUtility
    */
   public BooleanFieldHandler() {
   }

   /**
    * Set the annotation containing specific detail for the field type handler
    *
    * @param annotation Annotation from field
    */
   @Override
   public void setAnnotation(Annotation annotation) {
      MapBoolean mb = (MapBoolean) annotation;
      bit = mb.bit();
      incrementOffset = mb.incrementOffset();
   }

   /**
    * Write the given object using the field type handler into the buffer
    *
    * @param value Object representing the value to write
    * @param buffer Buffer to write to. Will use current position and enforce
    * limit
    *
    * @throws MemoryMapException Error writing the value to the buffer
    */
   @Override
   public void write(Object value, ByteBuffer buffer) throws MemoryMapException {
      // Make sure there are enough bytes available 
      if (buffer.remaining() < 1) {
         throw new MemoryMapException("Not enough bytes remaining in buffer to write boolean value.");
      }

      // Make sure value is Boolean type
      if (!(value instanceof Boolean)) {
         throw new MemoryMapException("Specified value is not of type Boolean");
      }

      // Note: ByteBuffer is 0'd when created
      // Merge the value into the current position
      byte b = buffer.get(buffer.position());  // Absolute get of current byte

      boolean gotoNextByte = false;
      // Create the byte val
      boolean val = (Boolean) value;
      if (BooleanBit.BIT_ALL != bit) {
         if (val) {
            // True, OR bit on
            b |= bit.getOnCmpMask();
         }
         gotoNextByte = true;
      } else {
         if (val) {
            b |= (1 << bitNum);
         }
         if (++bitNum >= 8) {
            bitNum = 0;
            gotoNextByte = true;
         }
      }

      // Write 1 byte to current buffer position
      buffer.put(buffer.position(), b);   // Absolute put

      // Move buffer forward 1 byte if required
      if (incrementOffset && gotoNextByte) {
         buffer.position(buffer.position() + 1);
      }
   }

   /**
    * Read a value using the field type handler from the buffer
    *
    * @param buffer Buffer to read value from current position and enforce limit
    * @return Object created
    * @throws MemoryMapException Error reading value
    */
   @Override
   public Object read(ByteBuffer buffer) throws MemoryMapException {
      // Make sure there are enough bytes available
      if (buffer.remaining() < 1) {
         throw new MemoryMapException("Not enough bytes remaining in buffer to read boolean value.");
      }

      byte b = buffer.get(buffer.position());  // Absolute get of current byte

      boolean gotoNextByte = false;
      if (BooleanBit.BIT_ALL != bit) {
         // Check the specific bit in the byte
         b &= bit.getOnCmpMask();
         gotoNextByte = true;
      } else {
         b &= (1 << bitNum);
         if (++bitNum >= 8) {
            bitNum = 0;
            gotoNextByte = true;
         }
      }

      // Create result
      Boolean res = null;
      // Note, both +ve and -ve values means a bit is set, so treating bit set as boolean true
      if (b != 0) {
         res = new Boolean(true);
      } else {
         res = new Boolean(false);
      }

      // Move buffer forward 1 byte if required
      if (incrementOffset && gotoNextByte) {
         buffer.position(buffer.position() + 1);
      }

      return res;
   }

   /**
    * Compare a constant value to a read value and throw exception if not equal
    *
    * @param constant Constant value to compare
    * @param value Read value to compare
    *
    */
   @Override
   public boolean compareConstant(Object constant, Object value) {
      if (!(constant instanceof Boolean) || !(value instanceof Boolean)) {
         return false;
      }

      return (((Boolean) constant).booleanValue() == ((Boolean) value).booleanValue());
   }
}
