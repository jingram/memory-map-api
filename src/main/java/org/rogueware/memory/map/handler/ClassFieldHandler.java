/*
 * ClassFieldHandler.java
 *
 * Defines a class used to handle reading and writing class fields
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map.handler;

import java.lang.annotation.Annotation;
import java.nio.ByteBuffer;
import org.rogueware.memory.map.MemoryMap;
import org.rogueware.memory.map.MemoryMapException;
import org.rogueware.memory.map.annotation.MapClass;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */


public class ClassFieldHandler extends BaseFieldHandler {

   String className;

   /**
    * Creates a new instance of MapBooleanUtility
    */
   public ClassFieldHandler() {
   }

   /**
    * Set the annotation containing specific detail for the field type handler
    *
    * @param annotation Annotation from field
    */
   @Override
   public void setAnnotation(Annotation annotation) {
      MapClass mc = (MapClass) annotation;
      className = mc.className();
   }

   /**
    * Write the given object using the field type handler into the buffer
    *
    * @param value Object representing the value to write
    * @param buffer Buffer to write to. Will use current position and enforce
    * limit
    *
    * @throws MemoryMapException Error writing the value to the buffer
    */
   @Override
   public void write(Object value, ByteBuffer buffer) throws MemoryMapException {
      // Recursivly encode the class
      ByteBuffer res = MemoryMap.encode(value);

      if (buffer.remaining() < res.remaining()) {
         throw new MemoryMapException("Not enough bytes remaining in buffer to write class value.");
      }

      buffer.put(res);
   }

   /**
    * Read a value using the field type handler from the buffer
    *
    * @param buffer Buffer to read value from current position and enforce limit
    * @return Object created
    * @throws MemoryMapException Error reading value
    */
   @Override
   public Object read(ByteBuffer buffer) throws MemoryMapException {
      // Recursivly decode the class

      Object res;
      try {
         res = MemoryMap.decode(Class.forName(className), buffer);
      } catch (ClassNotFoundException ex) {
         throw new MemoryMapException("Unable to resolve class name " + className);
      }

      return res;
   }

   /**
    * Compare a constant value to a read value and throw exception if not equal
    *
    * @param constant Constant value to compare
    * @param value Read value to compare
    *
    */
   @Override
   public boolean compareConstant(Object constant, Object value) {
      // Cannot have a class as a constant!!!
      return false;
   }
}
