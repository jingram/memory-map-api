/*
 * FieldHandlerInterface.java
 *
 * Defines an interface all field handlers are required to implement
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map.handler;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import org.rogueware.memory.map.MemoryMapException;
import org.rogueware.memory.map.enums.ByteOrder;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public interface FieldHandlerInterface {

   /**
    * Set the field value for introspection purposes
    *
    * @param field The field
    */
   public void setField(Field field);

   /**
    * Set the annotation containing specific detail for the field type handler
    *
    * @param annotation Annotation from field
    */
   public void setAnnotation(Annotation annotation);

   /**
    * Set the default byte order to use
    *
    * @param def Default byte order
    */
   public void setDefaulByteOrder(ByteOrder def);

   /**
    * Set the instance containing the field values and definitions
    *
    * @param structure Instance of the structure
    */
   public void setStructureInstance(Object structure);

   /**
    * Write the given object using the field type handler into the buffer
    *
    * @param value Object representing the value to write
    * @param buffer Buffer to write to. Will use current position and enforce
    * limit
    *
    * @throws MemoryMapException Error writing the value to the buffer
    */
   public void write(Object value, ByteBuffer buffer) throws MemoryMapException;

   /**
    * Read a value using the field type handler from the buffer
    *
    * @param buffer Buffer to read value from current position and enforce limit
    * @return Object created
    * @throws MemoryMapException Error reading value
    */
   public Object read(ByteBuffer buffer) throws MemoryMapException;

   /**
    * Compare a constant value to a read value and throw exception if not equal
    *
    * @param constant Constant value to compare
    * @param value Read value to compare
    *
    * @return Returns true if the constant value matches the read value
    */
   public boolean compareConstant(Object constant, Object value);

}
