/*
 * HandlerFactory.java
 *
 * Defines a class used to as a factory class to return a type handler given field criterial
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map.handler;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import org.rogueware.memory.map.MemoryMapException;
import org.rogueware.memory.map.annotation.MapBoolean;
import org.rogueware.memory.map.annotation.MapClass;
import org.rogueware.memory.map.annotation.MapDate;
import org.rogueware.memory.map.annotation.MapNumeric;
import org.rogueware.memory.map.annotation.MapString;
import org.rogueware.memory.map.enums.ByteOrder;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class FieldHandlerFactory {

   /**
    * Method to determine which field handler to use based on field criteria and
    * annotations
    *
    * @param defByteOrder Default structure byte order
    * @param field Field to obtain the handler for
    * @param structure Structure instance containing the field definition
    * @return Handler that can process read / write requestes
    * @throws MemoryMapException On field retrival error
    */
   public static FieldHandlerInterface getFieldHandler(ByteOrder defByteOrder, Field field, Object structure) throws MemoryMapException {
      // Determine the field type and instantiate a class handler initialised with required values
      FieldHandlerInterface handler = null;

      Annotation typeAnnotation;

      if (null != (typeAnnotation = field.getAnnotation(MapBoolean.class))) {
         // Boolean
         handler = new BooleanFieldHandler();
      } else if (null != (typeAnnotation = field.getAnnotation(MapClass.class))) {
         // Class
         handler = new ClassFieldHandler();
      } else if (null != (typeAnnotation = field.getAnnotation(MapDate.class))) {
         // Date
         handler = new DateFieldHandler();
      } else if (null != (typeAnnotation = field.getAnnotation(MapNumeric.class))) {
         // Date
         handler = new NumericFieldHandler();
      } else if (null != (typeAnnotation = field.getAnnotation(MapString.class))) {
         // Date
         handler = new StringFieldHandler();
      } else {
         throw new MemoryMapException(" field='" + field.getName() + "' unable to obtain field type handler");
      }

      // Set the field data for the handler
      handler.setField(field);

      // Set the annotation data for the handler
      handler.setAnnotation(typeAnnotation);

      // Set the default byte order on the handler
      handler.setDefaulByteOrder(defByteOrder);

      // Set the structure
      handler.setStructureInstance(structure);

      return handler;
   }

   /**
    * Determine the byte order to use for a field given the default structure
    * order the field belongs to
    *
    * @param structureOrder Byte order of structure field belongs to
    * @param fieldOrder Byte order of field
    * @return The calculated byte order to use
    */
   public static java.nio.ByteOrder determineByteOrder(ByteOrder structureOrder, ByteOrder fieldOrder) {
      java.nio.ByteOrder order;

      // Determine structure order
      if (ByteOrder.DEFAULT == structureOrder) {
         // Use native default if DEFAULT specified
         order = java.nio.ByteOrder.nativeOrder();
      } else {
         // Use specified default of structure
         if (ByteOrder.LITTLE_ENDIAN == structureOrder) {
            order = java.nio.ByteOrder.LITTLE_ENDIAN;
         } else {
            order = java.nio.ByteOrder.BIG_ENDIAN;
         }
      }

      // Overide structure default with field default if specified
      if (ByteOrder.DEFAULT == fieldOrder) {
         return order;     // Use the structure byte order
      }
      if (ByteOrder.LITTLE_ENDIAN == fieldOrder) {
         order = java.nio.ByteOrder.LITTLE_ENDIAN;
      } else {
         order = java.nio.ByteOrder.BIG_ENDIAN;
      }

      return order;
   }

}
