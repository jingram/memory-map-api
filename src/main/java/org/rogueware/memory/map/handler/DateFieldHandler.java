/*
 * DateFieldHandler.java
 *
 * Defines a class used to handle reading and writing date fields
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map.handler;

import java.lang.annotation.Annotation;
import java.nio.ByteBuffer;
import java.util.Calendar;
import java.util.Date;
import org.rogueware.memory.map.MemoryMapException;
import org.rogueware.memory.map.annotation.MapDate;
import org.rogueware.memory.map.enums.AtomicElementSize;
import org.rogueware.memory.map.enums.ByteOrder;
import org.rogueware.memory.map.enums.DateEpoch;
import org.rogueware.memory.map.enums.DateSize;
import org.rogueware.memory.map.util.EndianUtil;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class DateFieldHandler extends BaseFieldHandler {

   private DateEpoch epoch;
   private DateSize size;
   private ByteOrder byteOrder;
   private AtomicElementSize atomicElementSize;

   /**
    * Creates a new instance of MapBooleanUtility
    */
   public DateFieldHandler() {
   }

   /**
    * Set the annotation containing specific detail for the field type handler
    *
    * @param annotation Annotation from field
    */
   @Override
   public void setAnnotation(Annotation annotation) {
      MapDate md = (MapDate) annotation;
      epoch = md.epoch();
      size = md.size();
      byteOrder = md.byteOrder();
      atomicElementSize = md.atomicElementSize();
   }

   /**
    * Write the given object using the field type handler into the buffer
    *
    * @param value Object representing the value to write
    * @param buffer Buffer to write to. Will use current position and enforce
    * limit
    *
    * @throws MemoryMapException Error writing the value to the buffer
    */
   @Override
   public void write(Object value, ByteBuffer buffer) throws MemoryMapException {
      if (buffer.remaining() < size.getByteCount()) {
         throw new MemoryMapException("Not enough bytes remaining in buffer to write date value.");
      }

      // Make sure value is Date type
      if (!(value instanceof Date)) {
         throw new MemoryMapException("Specified value is not of type Date");
      }

      // Create timestamp value based on epoch settings
      Date val = (Date) value;
      long timeStamp = createTimestamp(val);

      // Write value based on specified size
      switch (size) {
         case UNSIGNED_32BIT:
            buffer.put(EndianUtil.marshalUnsigned32(timeStamp, FieldHandlerFactory.determineByteOrder(defaultByteOrder, byteOrder), atomicElementSize));
            break;

         case SIGNED_64BIT:
            buffer.put(EndianUtil.marshalSigned64(timeStamp, FieldHandlerFactory.determineByteOrder(defaultByteOrder, byteOrder)));
            break;
      }
   }

   /**
    * Read a value using the field type handler from the buffer
    *
    * @param buffer Buffer to read value from current position and enforce limit
    * @return Object created
    * @throws MemoryMapException Error reading value
    */
   @Override
   public Object read(ByteBuffer buffer) throws MemoryMapException {
      if (buffer.remaining() < size.getByteCount()) {
         throw new MemoryMapException("Not enough bytes remaining in buffer to read date value.");
      }

      // Read value based on specified size and unmarshal
      long timeStamp = 0;
      byte byteBuf[] = new byte[size.getByteCount()];
      buffer.get(byteBuf);
      switch (size) {
         case UNSIGNED_32BIT:
            timeStamp = EndianUtil.unmarshalUnsigned32(byteBuf, FieldHandlerFactory.determineByteOrder(defaultByteOrder, byteOrder), atomicElementSize);
            break;

         case SIGNED_64BIT:
            timeStamp = EndianUtil.unmarshalSigned64(byteBuf, FieldHandlerFactory.determineByteOrder(defaultByteOrder, byteOrder));
            break;
      }

      Date date = createDate(timeStamp);
      return date;
   }

   /**
    * Compare a constant value to a read value and throw exception if not equal
    *
    * @param constant Constant value to compare
    * @param value Read value to compare
    *
    */
   @Override
   public boolean compareConstant(Object constant, Object value) {
      if (!(constant instanceof Date) || !(value instanceof Date)) {
         return false;
      }

      return (((Date) constant).getTime() == ((Date) value).getTime());
   }

   /**
    * Internal method to create timestamp from Date object for specified epoch
    *
    * @param val
    * @return
    *
    * @throws MemoryMapException Error reading value
    */
   private long createTimestamp(Date val) throws MemoryMapException {
      long timeStamp = val.getTime();   // In milliseconds

      // Special case conversion based on custom types (overide value of timstamp)
      switch (epoch) {
         case BCD_DAY_TIME:
            // Special case. This is not units since epoch
            //               Covert to seconds to be used against Java Epoch interpreting as local time
            // Get the timestamp components (Each one is a nibble as BCD econded in 32bit timestamp. d HH mm ss)
            Calendar bcdDayTime = Calendar.getInstance();
            bcdDayTime.setTime(val);

            // Sanity check
            Calendar now = Calendar.getInstance();
            long nowMonths = (now.get(Calendar.YEAR) * 12) + now.get(Calendar.MONTH);
            long bcdMonths = (bcdDayTime.get(Calendar.YEAR) * 12) + bcdDayTime.get(Calendar.MONTH);
            if (now.after(bcdDayTime)) {
               if (nowMonths - bcdMonths > 1) {
                  throw new MemoryMapException("BCD Day Time cannot be < 1 month of current date");
               }
            } else {
               if (bcdMonths - nowMonths > 1) {
                  throw new MemoryMapException("BCD Day Time cannot be > 1 month of current date");
               }
            }

            int timeStampSeconds = bcdDayTime.get(Calendar.SECOND);
            int timeStampMinutes = bcdDayTime.get(Calendar.MINUTE);
            int timeStampHours = bcdDayTime.get(Calendar.HOUR_OF_DAY);
            int timeStampDay = bcdDayTime.get(Calendar.DAY_OF_MONTH);

            timeStamp = EndianUtil.marshalBCDByteValue(timeStampDay);
            timeStamp <<= 8;
            timeStamp += EndianUtil.marshalBCDByteValue(timeStampHours);
            timeStamp <<= 8;
            timeStamp += EndianUtil.marshalBCDByteValue(timeStampMinutes);
            timeStamp <<= 8;
            timeStamp += EndianUtil.marshalBCDByteValue(timeStampSeconds);

            break;
      }

      // Convert to units specified by epoch
      switch (epoch.getUnit()) {
         case SECONDS:
            timeStamp /= 1000;          // Convert milliseconds to seconds
            break;

         case MILLISECONDS:
            // Do nothing (Timestamp already in milliseconds
            break;

         case HECTONANOSECONDS:
            timeStamp *= 10000L;        // Convert milliseconds to hectonanoseconds (100 nanosecond)
            break;

         case NANOSECONDS:
            timeStamp *= 1000000L;      // Convert milliseconds to nanoseconds
      }

      // Add the offset to the timstamp to align to specified epoch
      timeStamp += epoch.getEpochOffset();

      return timeStamp;
   }

   /**
    * Internal method to create Date object from timestamp for specified epoch
    *
    * @param timestamp Timestamp to be interpreted for epoch
    * @return Date object created
    */
   private Date createDate(long timeStamp) {
      // Remopve the offset from the timstamp to align to JAVA epoch
      timeStamp -= epoch.getEpochOffset();

      // Special case conversion based on custom types (overide value of timstamp)
      switch (epoch) {
         case BCD_DAY_TIME:
            // Special case. This is not units since epoch
            //               Covert to seconds to be used against Java Epoch interpreting as local time
            // Get the timestamp components (Each one is a nibble as BCD econded in 32bit timestamp. d HH mm ss)
            int timeStampSeconds = EndianUtil.unmarshalBCDByteValue((int) timeStamp);
            int timeStampMinutes = EndianUtil.unmarshalBCDByteValue(((int) timeStamp >> 8));
            int timeStampHours = EndianUtil.unmarshalBCDByteValue(((int) timeStamp >> 16));
            int timeStampDay = EndianUtil.unmarshalBCDByteValue(((int) timeStamp >> 24));

            Calendar bcdDayTime = Calendar.getInstance();
            bcdDayTime.set(Calendar.DAY_OF_MONTH, timeStampDay);
            bcdDayTime.set(Calendar.HOUR_OF_DAY, timeStampHours);
            bcdDayTime.set(Calendar.MINUTE, timeStampMinutes);
            bcdDayTime.set(Calendar.SECOND, timeStampSeconds);
            bcdDayTime.set(Calendar.MILLISECOND, 0);

            // Get components of current date
            Calendar now = Calendar.getInstance();  // Local time

            if (bcdDayTime.after(now)) {
               bcdDayTime.add(Calendar.MONTH, -1);
            }

            timeStamp = bcdDayTime.getTimeInMillis();

            break;
      }

      // Convert to JAVA millisecons
      switch (epoch.getUnit()) {
         case SECONDS:
            timeStamp *= 1000L;    // Convert seconds to milliseconds
            break;

         case MILLISECONDS:
            // Do nothing (Timestamp already in milliseconds
            break;

         case HECTONANOSECONDS:
            timeStamp /= 10000L;    // Convert hectonanoseconds (100 nanoseconds) to milliseconds
            break;

         case NANOSECONDS:
            timeStamp /= 1000000L;  // Convert nanoseconds to milliseconds
      }

      return new Date(timeStamp);
   }

}
