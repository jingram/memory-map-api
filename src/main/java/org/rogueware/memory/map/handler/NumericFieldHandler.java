/*
 * NumericFieldHandler.java
 *
 * Defines a class used to handle reading and writing numeric fields
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map.handler;

import java.lang.annotation.Annotation;
import java.nio.ByteBuffer;
import org.rogueware.memory.map.MemoryMapException;
import org.rogueware.memory.map.annotation.MapNumeric;
import org.rogueware.memory.map.enums.AtomicElementSize;
import org.rogueware.memory.map.enums.ByteOrder;
import org.rogueware.memory.map.enums.NumericType;
import org.rogueware.memory.map.util.EndianUtil;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class NumericFieldHandler extends BaseFieldHandler {

   private NumericType type;
   private ByteOrder byteOrder;
   private AtomicElementSize atomicElementSize;

   /**
    * Creates a new instance of MapBooleanUtility
    */
   public NumericFieldHandler() {
   }

   /**
    * Set the annotation containing specific detail for the field type handler
    *
    * @param annotation Annotation from field
    */
   @Override
   public void setAnnotation(Annotation annotation) {
      MapNumeric mn = (MapNumeric) annotation;
      type = mn.type();
      byteOrder = mn.byteOrder();
      atomicElementSize = mn.atomicElementSize();
   }

   /**
    * Write the given object using the field type handler into the buffer
    *
    * @param value Object representing the value to write
    * @param buffer Buffer to write to. Will use current position and enforce
    * limit
    *
    * @throws MemoryMapException Error writing the value to the buffer
    */
   @Override
   public void write(Object value, ByteBuffer buffer) throws MemoryMapException {
      if (buffer.remaining() < type.getByteCount()) {
         throw new MemoryMapException("Not enough bytes remaining in buffer to write numeric value.");
      }

      // Make sure value is Boolean type
      if (!(value instanceof Byte || value instanceof Short || value instanceof Integer
            || value instanceof Long || value instanceof Float || value instanceof Double)) {
         throw new MemoryMapException("Specified value is not of supported numeric type");
      }

      long intVal;
      double floatVal;

      // Read value from object correctly
      java.nio.ByteOrder order = FieldHandlerFactory.determineByteOrder(defaultByteOrder, byteOrder);
      Number val = (Number) value;

      // Write given type based on defined output value
      switch (type) {
         case SIGNED_8BIT:
            buffer.put(val.byteValue());
            break;

         case UNSIGNED_8BIT:
            buffer.put(EndianUtil.marshalUnsigned8(val.shortValue()));
            break;

         case SIGNED_16BIT:
            buffer.put(EndianUtil.marshalSigned16(val.shortValue(), order));
            break;

         case UNSIGNED_16BIT:
            buffer.put(EndianUtil.marshalUnsigned16(val.intValue(), order));
            break;

         case SIGNED_32BIT:
            buffer.put(EndianUtil.marshalSigned32(val.intValue(), order, atomicElementSize));
            break;

         case UNSIGNED_32BIT:
            buffer.put(EndianUtil.marshalUnsigned32(val.longValue(), order, atomicElementSize));
            break;

         case SIGNED_64BIT:
            buffer.put(EndianUtil.marshalSigned64(val.longValue(), order));
            break;

         case IEEE_754_32BIT:
            buffer.put(EndianUtil.marshalIEEE754_32(val.floatValue(), order, atomicElementSize));
            break;

         case IEEE_754_64BIT:
            buffer.put(EndianUtil.marshalIEEE754_64(val.doubleValue(), order));
            break;
      }
   }

   /**
    * Read a value using the field type handler from the buffer
    *
    * @param buffer Buffer to read value from current position and enforce limit
    * @return Object created
    * @throws MemoryMapException Error reading value
    */
   @Override
   public Object read(ByteBuffer buffer) throws MemoryMapException {
      if (buffer.remaining() < type.getByteCount()) {
         throw new MemoryMapException("Not enough bytes remaining in buffer to read numeric value.");
      }

      // Read the bytes
      byte byteBuf[] = new byte[type.getByteCount()];
      buffer.get(byteBuf);

      // Create the correct result object
      java.nio.ByteOrder order = FieldHandlerFactory.determineByteOrder(defaultByteOrder, byteOrder);
      Number value = null;

      long intVal = 0;
      double floatVal = 0.0d;
      switch (type) {
         case SIGNED_8BIT:
            intVal = byteBuf[0];
            break;

         case UNSIGNED_8BIT:
            intVal = EndianUtil.unmarshalUnsigned8(byteBuf[0]);
            break;

         case SIGNED_16BIT:
            intVal = EndianUtil.unmarshalSigned16(byteBuf, order);
            break;

         case UNSIGNED_16BIT:
            intVal = EndianUtil.unmarshalUnsigned16(byteBuf, order);
            break;

         case SIGNED_32BIT:
            intVal = EndianUtil.unmarshalSigned32(byteBuf, order, atomicElementSize);
            break;

         case UNSIGNED_32BIT:
            intVal = EndianUtil.unmarshalUnsigned32(byteBuf, order, atomicElementSize);
            break;

         case SIGNED_64BIT:
            intVal = EndianUtil.unmarshalSigned64(byteBuf, order);
            break;

         case IEEE_754_32BIT:
            floatVal = EndianUtil.unmarshalIEEE754_32(byteBuf, order, atomicElementSize);
            break;

         case IEEE_754_64BIT:
            floatVal = EndianUtil.unmarshalIEEE754_64(byteBuf, order);
            break;
      }

      // Create the correct java object for the destintion field
      if ("byte".equals(fieldType)) {
         value = new Byte((byte) intVal);
      } else if ("short".equals(fieldType)) {
         value = new Short((short) intVal);
      } else if ("int".equals(fieldType)) {
         value = new Integer((int) intVal);
      } else if ("long".equals(fieldType)) {
         value = new Long(intVal);
      } else if ("float".equals(fieldType)) {
         value = new Float((float) floatVal);
      } else if ("double".equals(fieldType)) {
         value = new Double(floatVal);
      }

      return value;
   }

   /**
    * Compare a constant value to a read value and throw exception if not equal
    *
    * @param constant Constant value to compare
    * @param value Read value to compare
    *
    */
   @Override
   public boolean compareConstant(Object constant, Object value) {
      if ((constant instanceof Byte) && (value instanceof Byte)) {
         return ((Byte) constant).byteValue() == ((Byte) value).byteValue();
      }

      if ((constant instanceof Short) && (value instanceof Short)) {
         return ((Short) constant).shortValue() == ((Short) value).shortValue();
      }

      if ((constant instanceof Integer) && (value instanceof Integer)) {
         return ((Integer) constant).intValue() == ((Integer) value).intValue();
      }

      if ((constant instanceof Long) && (value instanceof Long)) {
         return ((Long) constant).longValue() == ((Long) value).longValue();
      }

      if ((constant instanceof Float) && (value instanceof Float)) {
         return ((Float) constant).floatValue() == ((Float) value).floatValue();
      }

      if ((constant instanceof Double) && (value instanceof Double)) {
         return ((Double) constant).doubleValue() == ((Double) value).doubleValue();
      }

      return false;
   }

}
