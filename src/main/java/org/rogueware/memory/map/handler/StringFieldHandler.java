/*
 * StringFieldHandler.java
 *
 * Defines a class used to handle reading and writing string fields
 *
 * Resources
 *   http://www.cl.cam.ac.uk/~mgk25/unicode.html#utf-8
 *   http://en.wikipedia.org/wiki/UTF-8
 *   http://en.wikipedia.org/wiki/UTF-16/UCS-2
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map.handler;

import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.rogueware.memory.map.MemoryMap;
import org.rogueware.memory.map.MemoryMapException;
import org.rogueware.memory.map.annotation.MapString;
import org.rogueware.memory.map.enums.StringType;
import org.rogueware.memory.map.util.EndianUtil;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class StringFieldHandler extends BaseFieldHandler {

   private String charset;
   private StringType type;
   private String stringSizeMember;
   private int stringSize;
   private String terminator;
   private byte terminatorCharset[];   // Raw bytes representing terminator encoded as character set
   private boolean swapAlternatingCharacters;

   /**
    * Creates a new instance of MapBooleanUtility
    */
   public StringFieldHandler() {
   }

   /**
    * Set the annotation containing specific detail for the field type handler
    *
    * @param annotation Annotation from field
    */
   @Override
   public void setAnnotation(Annotation annotation) {
      MapString ms = (MapString) annotation;
      charset = ms.charset();
      type = ms.type();
      stringSizeMember = ms.stringSizeMember();
      stringSize = ms.stringSize();
      terminator = ms.terminator();
      swapAlternatingCharacters = ms.swapAlternatingCharacters();

      // Create the correctly encoded terminator string for searching raw bytes
      try {
         terminatorCharset = terminator.getBytes(charset);

         // Remove BE LE bytes from UTF-16 as this is appended to string
         if ("UTF-16".equals(charset)) {
            terminatorCharset = Arrays.copyOfRange(terminatorCharset, 2, terminatorCharset.length);
         }
      } catch (UnsupportedEncodingException ex) {
         terminatorCharset = new byte[0];   // No terminator as failed to encode
      }
   }

   /**
    * Write the given object using the field type handler into the buffer
    *
    * @param value Object representing the value to write
    * @param buffer Buffer to write to. Will use current position and enforce
    * limit
    *
    * @throws MemoryMapException Error writing the value to the buffer
    */
   @Override
   public void write(Object value, ByteBuffer buffer) throws MemoryMapException {
      // Make sure value is String type
      if (!(value instanceof String)) {
         throw new MemoryMapException("Specified value is not of type String");
      }

      String val = (String) value;

      // Get the size for fixed string and padded if required
      int fixedSize = 0;
      if (StringType.FIXED == type) {
         if (stringSize > 0) {
            fixedSize = stringSize;
         } else {
            fixedSize = MemoryMap.getSizeMemberFieldValue(structure, stringSizeMember);
         }

         // Right Pad with spaces if string too small
         if (fixedSize > val.length()) {
            val = String.format("%1$-" + fixedSize + "s", val);
         }

         // Truncate if string too large
         if (val.length() > fixedSize) {
            val = val.substring(0, fixedSize);
         }
      }
      if (StringType.FIXED_NULL_TERMINATED == type) {
         if (stringSize > 0) {
            fixedSize = stringSize;
         } else {
            fixedSize = MemoryMap.getSizeMemberFieldValue(structure, stringSizeMember);
         }

         // Right Pad with null if string too small
         if (fixedSize > val.length()) {
            char[] tmp = val.toCharArray();
            char[] fixedString = new char[fixedSize];
            System.arraycopy(tmp, 0, fixedString, 0, tmp.length);
            val = new String(fixedString);
         }

         // Truncate if string too large
         if (val.length() > fixedSize) {
            val = val.substring(0, fixedSize);
         }
      }

      if (swapAlternatingCharacters) {
         // swap alternating characters. if there are an odd number of characters, leave the last character untouched.
         char[] chars = val.toCharArray();
         for (int i = 0; i < chars.length / 2; ++i) {
            char tmp = chars[i * 2];
            chars[i * 2] = chars[i * 2 + 1];
            chars[i * 2 + 1] = tmp;
         }
         val = new String(chars);
      }

      // Create raw bytes using character set
      byte raw[];
      try {
         raw = val.getBytes(charset);
      } catch (UnsupportedEncodingException ex) {
         throw new MemoryMapException("Unable to decode string into raw bytes using character set " + charset);
      }

      // Write the bytes based on the type of string
      switch (type) {

         case NULL_TERMINATED:
            if (buffer.remaining() < raw.length + 1) {
               throw new MemoryMapException("Not enough bytes remaining in buffer to write string value.");
            }

            buffer.put(raw);
            buffer.put((byte) 0);    // Write NULL terminating character

            // If UTF-16xx write an extra null terminating character
            if (charset.startsWith("UTF-16")) {
               buffer.put((byte) 0);    // Write NULL terminating character
            }
            break;

         case STRING_TERMINATED:
            if (buffer.remaining() < raw.length + terminatorCharset.length) {
               throw new MemoryMapException("Not enough bytes remaining in buffer to write string value.");
            }

            // Make sure the terminator does not appear in the string
            if (val.contains(terminator)) {
               throw new MemoryMapException("String value contains the terminator");
            }

            buffer.put(raw);
            buffer.put(terminatorCharset);  // Terminator
            break;

         case FIXED:
         case FIXED_NULL_TERMINATED:
            // Write a fixed string length with padding if was required
            buffer.put(raw);
            break;
      }
   }

   /**
    * Read a value using the field type handler from the buffer
    *
    * @param buffer Buffer to read value from current position and enforce limit
    * @return Object created
    * @throws MemoryMapException Error reading value
    */
   @Override
   public Object read(ByteBuffer buffer) throws MemoryMapException {
      // Read the bytes
      byte raw[] = null;
      int pos, offset;
      switch (type) {
         case NULL_TERMINATED:
            // Look for the null terminated char(s) in the stream to get the size of the bytes to read
            pos = buffer.position();
            offset = pos;

            // US-ASCII, UTF-8
            if ("US-ASCII".equals(charset) || "UTF-8".equals(charset)) {
               // US-ASCII and UTF-8
               while (pos < buffer.limit() && 0 != buffer.get(pos)) {
                  pos++;
               }

               // Null char not found
               if (pos >= buffer.limit()) {
                  throw new MemoryMapException("Buffer over run looking for NULL terminating character while reading US-ASCII / UTF-8 string");
               }

               raw = new byte[pos - offset];   // Size excluding null terminated char
               buffer.get(raw);
               buffer.get();          // Skip null terminated string
            } else if (charset.startsWith("UTF-16")) {
               // UTF-16, UTF-16BE and UTF-16LE

               // Look for 0x0 0x0 for null terminating string
               while (pos < buffer.limit() - 1 && (0 != buffer.get(pos) || 0 != buffer.get(pos + 1))) {
                  pos++;
               }

               // Null char not found
               if (pos >= buffer.limit() - 1) {
                  throw new MemoryMapException("Buffer over run looking for NULL terminating character while reading UTF-16 string");
               }

               raw = new byte[pos - offset];   // Size excluding null terminated char
               buffer.get(raw);
               buffer.get();          // Skip null terminated string
               buffer.get();          // Skip second null terminated string
            }

            break;

         case STRING_TERMINATED:
            // Look for terminator encoded byte sequence
            pos = buffer.position();
            offset = pos;
            boolean found = false;

            while (pos < buffer.limit() && !found) {
               // If the first byte matches, check the rest for a match if there are enough bytes
               if (terminatorCharset[0] == buffer.get(pos)) {
                  found = true;
                  for (int i = 1; i < terminatorCharset.length; i++) {
                     if (pos + i >= buffer.limit()) {
                        break;   // Run out of buffer bytes
                     }
                     if (terminatorCharset[i] != buffer.get(pos + i)) {
                        found = false;
                        break;
                     }
                  }
               }

               if (!found) {
                  pos++;   // Try next byte to look for sequence
               }
            }

            // Null char not found
            if (pos >= buffer.limit() || !found) {
               throw new MemoryMapException("Buffer over run looking for terminating character sequence while reading string");
            }

            raw = new byte[pos - offset];   // Size excluding null terminated char
            buffer.get(raw);
            buffer.position(buffer.position() + terminatorCharset.length);   // Skip terminating character sequence

            break;

         case FIXED:
         case FIXED_NULL_TERMINATED:
            int fixedSize = 0;
            if (stringSize > 0) {
               fixedSize = stringSize;
            } else {
               fixedSize = MemoryMap.getSizeMemberFieldValue(structure, stringSizeMember);
            }

            // Change fixed size based on character set
            if ("UTF-8".equals(charset)) {
               // Count bytes until got enough characters
               int count = 0;
               pos = buffer.position();
               offset = pos;

               while (pos < buffer.limit() && count != fixedSize) {
                  short uByte = EndianUtil.unmarshalUnsigned8(buffer.get(pos));

                  if (uByte >= 0x00 && uByte <= 0x7F) {
                     // 1 byte encoded character
                     count++;
                     pos++;
                  } else if (uByte >= 0xC2 && uByte <= 0xDF) {
                     // 2 byte encoded character
                     count++;
                     pos += 2;
                  } else if (uByte >= 0xE0 && uByte <= 0xEF) {
                     // 3 byte encoded character
                     count++;
                     pos += 3;
                  } else if (uByte >= 0xF0 && uByte <= 0xF4) {
                     // 4 byte encoded character
                     count++;
                     pos += 4;
                  } else if (uByte >= 0xF8 && uByte <= 0xFB) {
                     // 5 byte encoded character
                     count++;
                     pos += 5;
                  } else if (uByte >= 0xFC && uByte <= 0xFD) {
                     // 6 byte encoded character
                     count++;
                     pos += 6;
                  }
               }

               // Run out of space before fixed size read
               if (pos > buffer.limit()) {
                  throw new MemoryMapException("Buffer over run reading  UTF-8 characters for fixed size string");
               }

               fixedSize = pos - offset;
            } else if ("UTF-16".equals(charset)) {
               // UTF-16
               fixedSize = 2 + (2 * fixedSize);
            } else if (charset.startsWith("UTF-16")) {
               // UTF-16BE UTF-16LE
               fixedSize *= 2;
            }

            if (fixedSize > buffer.limit()) {
               throw new MemoryMapException("Not enough bytes remaining in buffer to read fixed string value.");
            }

            raw = new byte[fixedSize];
            buffer.get(raw);

            break;
      }

      // Decode the raw bytes into a string
      String val;
      try {
         val = new String(raw, charset);
         if (swapAlternatingCharacters) {
            // swap alternating characters. if there are an odd number of characters, leave the last character untouched.
            char[] chars = val.toCharArray();
            for (int i = 0; i < chars.length / 2; ++i) {
               char tmp = chars[i * 2];
               chars[i * 2] = chars[i * 2 + 1];
               chars[i * 2 + 1] = tmp;
            }
            val = new String(chars);
         }
         if (StringType.FIXED_NULL_TERMINATED == type) {
            int i = val.indexOf('\0');
            if (-1 != i) {
               val = val.substring(0, i);
            }
         }
      } catch (UnsupportedEncodingException ex) {
         throw new MemoryMapException("Unable to decode string from raw bytes using character set " + charset);
      }

      return val;
   }

   /**
    * Compare a constant value to a read value and throw exception if not equal
    *
    * @param constant Constant value to compare
    * @param value Read value to compare
    *
    */
   @Override
   public boolean compareConstant(Object constant, Object value) {
      if (!(constant instanceof String) || !(value instanceof String)) {
         return false;
      }

      return (((String) constant).equals((String) value));
   }
}
