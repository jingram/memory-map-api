/*
 * ByteOrder.java
 *
 * Defines a class used to define the numeric byte order
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map.enums;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public enum ByteOrder {

   BIG_ENDIAN, // The most significant byte (MSB) value is stored at the memory location with the lowest address
   LITTLE_ENDIAN, // The least significant byte (LSB) value is stored at the memory location with the lowest address
   // TODO PDP_ENDIAN,          // Storing the most significant "half" (16-bits) followed by the less significant half (as if big-endian) but with each half stored in little-endian format. This ordering is known as PDP-endianness.
   DEFAULT
}
