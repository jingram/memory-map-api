/*
 * DateEpoch.java
 *
 * Defines a class used to enum to describe available date epoch's
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map.enums;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public enum DateEpoch {

   JAVA(0, DateUnit.MILLISECONDS), // January 1, 1970 00:00:00   1 millisecond per tick
   UNIX(0, DateUnit.SECONDS), // January 1, 1970 00:00:00   1 second per tick
   UNIX_MICROSECONDS(0, DateUnit.MILLISECONDS), // January 1, 1970 00:00:00   1 millisecond per tick
   NTFS_WIN32_WIN64(116444736000000000L, DateUnit.HECTONANOSECONDS), // January 1, 1601 00:00:00   100 nanoseconds per tick (11644473600 seconds to Unix epoch)
   DOT_NET(621355968000000000L, DateUnit.HECTONANOSECONDS), // January 1, 0001 00:00:00   100 nanoseconds per tick (62135596800 seconds to Unix epoch)
   BCD_DAY_TIME(0L, DateUnit.MILLISECONDS);                           // Custom format with epoch being the start of the day specified in the date

   private long epochOffset;
   private DateUnit unit;

   private DateEpoch(long epochOffset, DateUnit offsetUnit) {
      this.epochOffset = epochOffset;
      this.unit = offsetUnit;
   }

   /**
    * @return the epochOffset
    */
   public long getEpochOffset() {
      return epochOffset;
   }

   /**
    * @return the unit
    */
   public DateUnit getUnit() {
      return unit;
   }
}
