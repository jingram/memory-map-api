/*
 * BooleanBit.java
 *
 * Defines a class used to define an enum for bits in a boolean value
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map.enums;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public enum BooleanBit {

   BIT_0(0x01, 0xFE), // on:00000001 off:11111110
   BIT_1(0x02, 0xFD), // on:00000010 off:11111101
   BIT_2(0x04, 0xFB), // on:00000100 off:11111011
   BIT_3(0x08, 0xF7), // on:00001000 off:11110111
   BIT_4(0x10, 0xEF), // on:00010000 off:11101111
   BIT_5(0x20, 0xDF), // on:00100000 off:11011111
   BIT_6(0x40, 0xBF), // on:01000000 off:10111111
   BIT_7(0x80, 0x7F), // on:10000000 off:01111111
   BYTE(0xFF, 0x00), // on:11111111 off:00000000
   BIT_ALL(0x00, 0x00); // Special case, mask not used

   private int onCmpMask;
   private int offMask;

   BooleanBit(int onCmpMask, int offMask) {
      this.onCmpMask = onCmpMask;
      this.offMask = offMask;
   }

   /**
    * @return the onCmpMask
    */
   public int getOnCmpMask() {
      return onCmpMask;
   }

   /**
    * @return the offMask
    */
   public int getOffMask() {
      return offMask;
   }
}
