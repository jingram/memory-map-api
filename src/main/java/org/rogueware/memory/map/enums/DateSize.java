/*
 * DateSize.java
 *
 * Defines a class used to define enum for date byte size
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map.enums;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public enum DateSize {

   UNSIGNED_32BIT(4),
   SIGNED_64BIT(8);

   private int byteCount;

   private DateSize(int byteCount) {
      this.byteCount = byteCount;
   }

   /**
    * @return the byteCount
    */
   public int getByteCount() {
      return byteCount;
   }
}
