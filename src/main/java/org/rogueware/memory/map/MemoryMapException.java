/*
 * MemoryMapException.java
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class MemoryMapException extends Exception {

   /**
    * Creates a new instance of MemoryMapException
    */
   public MemoryMapException() {
   }

   /**
    * Creates a new instance of <code>MemoryMapException</code> with the
    * specified detail message
    *
    * @param message String of the detail message
    *
    */
   public MemoryMapException(String message) {
      super(message);
   }

   /**
    * Creates a new instance of <code>MemoryMapException</code> with the
    * specified detail message
    *
    * @param cause Throwable class as cause
    *
    */
   public MemoryMapException(Throwable cause) {
      super(cause);
   }

   /**
    * Create a new instance of <code>MemoryMapException</code> with the
    * specified detail message and throwable class
    *
    * @param message String of the detail message
    * @param cause Throwable class as cause
    */
   public MemoryMapException(String message, Throwable cause) {
      super(message, cause);
   }
}
