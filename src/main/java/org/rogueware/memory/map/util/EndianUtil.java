/*
 * EndianUtil.java
 * Defines a class used to define utilities for converting numeric primitives between big and little endian
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map.util;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.rogueware.memory.map.enums.AtomicElementSize;

/**
 *
 * @author Tony Abbott
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class EndianUtil {

   /**
    * Creates a new instance of EndianUtil
    */
   private EndianUtil() {
   }

   /**
    * Marshal a byte into unsigned bit pattern
    *
    * @param value Value to marshal
    * @return byte reprenting unsigned byte bit pattern
    */
   public static final byte marshalUnsigned8(int value) {
      if (value < 0 || value > 255) {
         throw new IllegalArgumentException(value + " out of range for unsigned 8bit");
      }

      return (byte) (value < 128 ? value : value - 256);
   }

   /**
    * Marshal value into a signed 16bit byte array
    *
    * @param value value of the signed
    * @param order Big / Little Endian byte order
    * @return 2 byte array containing marshalled signed 16bit value
    */
   public static final byte[] marshalSigned16(short value, ByteOrder order) {
      byte data[] = new byte[2];
      ByteBuffer b = ByteBuffer.wrap(data);

      b.order(order);
      b.putShort(value);

      return data;
   }

   /**
    * Marshal value into unsigned 16 bit byte array
    *
    * @param value Value of the unsigned
    * @param order Big / Little Endian byte order
    * @return 2 byte array containing marshalled unsigned 16bit value
    */
   public static final byte[] marshalUnsigned16(int value, ByteOrder order) {
      byte data[] = new byte[2];

      if (value < 0 || value > 65535) {
         throw new IllegalArgumentException(value + " out of range for unsigned 16bit");
      }

      if (ByteOrder.LITTLE_ENDIAN == order) {
         // Little Endian
         data[1] = marshalUnsigned8((short) ((value & 0xff00) >> 8));
         data[0] = marshalUnsigned8((short) (value & 0xff));
      } else {
         // Big Endian
         data[0] = marshalUnsigned8((short) ((value & 0xff00) >> 8));
         data[1] = marshalUnsigned8((short) (value & 0xff));
      }

      return data;
   }

   /**
    * Marshal value into a signed 32bit byte array
    *
    * @param value value of the signed
    * @param order Big / Little Endian byte order
    * @return 4 byte array containing marshalled signed 32bit value
    */
   public static final byte[] marshalSigned32(int value, ByteOrder order) {
      return marshalSigned32(value, order, AtomicElementSize.ATOMIC_ELEMENT_8BIT); // 8 bit atomic element version of the function
   }

   /**
    * Marshal value into a signed 32bit byte array
    *
    * @param value Value of the unsigned
    * @param order Big / Little Endian byte order
    * @param atomicElementSize Atomic Element size of the architecture.
    * Determines how bytes are ordered
    * @return 4 byte array containing marshalled unsigned 16bit value
    */
   public static final byte[] marshalSigned32(int value, ByteOrder order, AtomicElementSize atomicElementSize) {
      // 8bit atomic element size operation
      byte data[] = new byte[4];
      ByteBuffer b = ByteBuffer.wrap(data);

      b.order(order);
      b.putInt(value);

      // Apply byte order depending on atomic element size
      switch (atomicElementSize) {
         case ATOMIC_ELEMENT_16BIT:
            // Order the bytes so they match 8bit operation handling
            swapBytes(data, 0, 1);
            swapBytes(data, 2, 3);
            break;
      }

      return data;
   }

   /**
    * Marshal value into an unsigned 32bit byte array
    *
    * @param value value of the unsigned
    * @param order Big / Little Endian byte order
    * @return 4 byte array containing marshalled unsigned 32bit value
    */
   public static final byte[] marshalUnsigned32(long value, ByteOrder order) {
      return marshalUnsigned32(value, order, AtomicElementSize.ATOMIC_ELEMENT_8BIT); // 8 bit atomic element version of the function
   }

   /**
    * Marshal value into an unsigned 32bit byte array
    *
    * @param value value of the unsigned
    * @param order Big / Little Endian byte order
    * @param atomicElementSize Atomic Element size of the architecture.
    * Determines how bytes are ordered
    * @return 4 byte array containing marshalled unsigned 32bit value
    */
   public static final byte[] marshalUnsigned32(long value, ByteOrder order, AtomicElementSize atomicElementSize) {
      byte data[] = new byte[4];

      if (value < 0 || value > 4294967295L) {
         throw new IllegalArgumentException(value + " out of range for unsigned 32bit");
      }

      if (ByteOrder.LITTLE_ENDIAN == order) {
         // Little Endian
         switch (atomicElementSize) {
            case ATOMIC_ELEMENT_16BIT:
               // 16bit atomic element size operation
               data[3] = marshalUnsigned8((short) ((value & 0xff0000) >> 16));
               data[2] = marshalUnsigned8((short) ((value & 0xff000000) >> 24));
               data[1] = marshalUnsigned8((short) (value & 0xff));
               data[0] = marshalUnsigned8((short) ((value & 0xff00) >> 8));
               break;

            default:
               // 8bit atomic element size operation
               data[3] = marshalUnsigned8((short) ((value & 0xff000000) >> 24));
               data[2] = marshalUnsigned8((short) ((value & 0xff0000) >> 16));
               data[1] = marshalUnsigned8((short) ((value & 0xff00) >> 8));
               data[0] = marshalUnsigned8((short) (value & 0xff));
               break;
         }
      } else {
         // Big Endian
         data[0] = marshalUnsigned8((short) ((value & 0xff000000) >> 24));
         data[1] = marshalUnsigned8((short) ((value & 0xff0000) >> 16));
         data[2] = marshalUnsigned8((short) ((value & 0xff00) >> 8));
         data[3] = marshalUnsigned8((short) (value & 0xff));
      }

      return data;
   }

   /**
    * Marshal value into a signed 64bit byte array
    *
    * @param value value of the signed
    * @param order Big / Little Endian byte order
    * @return 8 byte array containing marshalled signed 32bit value
    */
   public static final byte[] marshalSigned64(long value, ByteOrder order) {
      byte data[] = new byte[8];
      ByteBuffer b = ByteBuffer.wrap(data);

      b.order(order);
      b.putLong(value);

      return data;
   }

   /**
    * Marshal value into a IEEE 754 32bit byte array
    *
    * @param value value of the IEEE 754
    * @param order Big / Little Endian byte order
    * @return 4 byte array containing marshalled signed 32bit IEEE 754 value
    */
   public static final byte[] marshalIEEE754_32(float value, ByteOrder order) {
      return marshalIEEE754_32(value, order, AtomicElementSize.ATOMIC_ELEMENT_8BIT);// 8 bit atomic element version of the function
   }

   /**
    * Marshal value into a IEEE 754 32bit byte array
    *
    * @param value value of the IEEE 754
    * @param order Big / Little Endian byte order
    * @param atomicElementSize Atomic Element size of the architecture.
    * Determines how bytes are ordered
    * @return 4 byte array containing marshalled signed 32bit IEEE 754 value
    */
   public static final byte[] marshalIEEE754_32(float value, ByteOrder order, AtomicElementSize atomicElementSize) {
      // 8bit atomic element size operation
      byte data[] = new byte[4];
      ByteBuffer b = ByteBuffer.wrap(data);

      b.order(order);
      b.putFloat(value);

      // Apply byte order depending on atomic element size
      switch (atomicElementSize) {
         case ATOMIC_ELEMENT_16BIT:
            // Order the bytes so they match 8bit operation handling
            swapBytes(data, 0, 1);
            swapBytes(data, 2, 3);
            break;
      }

      return data;
   }

   /**
    * Marshal value into a IEEE 754 64bit byte array
    *
    * @param value value of the IEEE 754
    * @param order Big / Little Endian byte order
    * @return 8 byte array containing marshalled signed 64bit IEEE 754 value
    */
   public static final byte[] marshalIEEE754_64(double value, ByteOrder order) {
      byte data[] = new byte[8];
      ByteBuffer b = ByteBuffer.wrap(data);

      b.order(order);
      b.putDouble(value);

      return data;
   }

   /**
    * Unmarshal unsigned bit pattern into byte
    *
    * @param value byte containing unsigned bit pattern
    * @return Siged byte
    */
   public static final short unmarshalUnsigned8(byte value) {
      return value >= 0 ? value : (short) (value + 256);
   }

   /**
    * Unmarshal signed 16 bit from byte array
    *
    * @param data Containing 16 bit value
    * @param order Big / Little Endian byte order
    * @return Value of signed 16 bit
    */
   public static final short unmarshalSigned16(byte[] data, ByteOrder order) {
      if (data.length < 2) {
         throw new IllegalArgumentException("data must contain at least 2 bytes for signed 16bit");
      }

      ByteBuffer b = ByteBuffer.wrap(data);
      b.order(order);

      return b.getShort();
   }

   /**
    * Unmarshal unsigned 16 bit from byte array
    *
    * @param data Containing 16 bit value
    * @param order Big / Little Endian byte order
    * @return Value of unsigned 16 bit
    */
   public static final int unmarshalUnsigned16(byte[] data, ByteOrder order) {
      if (data.length < 2) {
         throw new IllegalArgumentException("data must contain at least 2 bytes for unsigned 16bit");
      }

      if (ByteOrder.LITTLE_ENDIAN == order) {
         // Little Endian
         return unmarshalUnsigned8(data[0])
               + (unmarshalUnsigned8(data[1]) << 8);
      } else {
         // Big Endian
         return unmarshalUnsigned8(data[1])
               + (unmarshalUnsigned8(data[0]) << 8);
      }
   }

   /**
    * Unmarshal signed 32 bit from byte array
    *
    * @param data Containing 16 bit value
    * @param order Big / Little Endian byte order
    * @return Value of signed 32 bit
    */
   public static final int unmarshalSigned32(byte[] data, ByteOrder order) {
      return unmarshalSigned32(data, order, AtomicElementSize.ATOMIC_ELEMENT_8BIT); // 8 bit atomic element version of the function
   }

   /**
    * Unmarshal signed 32 bit from byte array
    *
    * @param data Containing 16 bit value
    * @param order Big / Little Endian byte order
    * @param atomicElementSize Atomic Element size of the architecture.
    * Determines how bytes are ordered
    * @return Value of signed 32 bit
    */
   public static final int unmarshalSigned32(byte[] data, ByteOrder order, AtomicElementSize atomicElementSize) {
      if (data.length < 4) {
         throw new IllegalArgumentException("data must contain at least 4 bytes for signed 32bit");
      }

      // Apply byte order depending on atomic element size
      switch (atomicElementSize) {
         case ATOMIC_ELEMENT_16BIT:
            // Order the bytes so they match 8bit operation handling
            swapBytes(data, 0, 1);
            swapBytes(data, 2, 3);
            break;
      }

      // 8bit atomic element size operation
      ByteBuffer b = ByteBuffer.wrap(data);
      b.order(order);

      return b.getInt();
   }

   /**
    * Unmarshal unsigned 32 bit from byte array
    *
    * @param data Containing 32 bit value
    * @param order Big / Little Endian byte order
    * @return Value of unsigned 32 bit
    */
   public static final long unmarshalUnsigned32(byte[] data, ByteOrder order) {
      return unmarshalUnsigned32(data, order, AtomicElementSize.ATOMIC_ELEMENT_8BIT); // 8 bit atomic element version of the function
   }

   /**
    * Unmarshal unsigned 32 bit from byte array
    *
    * @param data Containing 32 bit value
    * @param order Big / Little Endian byte order
    * @param atomicElementSize Atomic Element size of the architecture.
    * Determines how bytes are ordered
    * @return Value of unsigned 32 bit
    */
   public static final long unmarshalUnsigned32(byte[] data, ByteOrder order, AtomicElementSize atomicElementSize) {
      if (data.length < 4) {
         throw new IllegalArgumentException("data must contain at least 4 bytes for unsigned 32bit");
      }

      if (ByteOrder.LITTLE_ENDIAN == order) {
         // Little Endian
         switch (atomicElementSize) {
            case ATOMIC_ELEMENT_16BIT:
               // 16bit atomic element size operation
               return (unmarshalUnsigned8(data[0]) << 8)
                     + unmarshalUnsigned8(data[1])
                     + ((long) unmarshalUnsigned8(data[2]) << 24)
                     + (unmarshalUnsigned8(data[3]) << 16);

            default:
               // 8bit atomic element size operation
               return unmarshalUnsigned8(data[0])
                     + (unmarshalUnsigned8(data[1]) << 8)
                     + (unmarshalUnsigned8(data[2]) << 16)
                     + ((long) unmarshalUnsigned8(data[3]) << 24);
         }
      } else {
         // Big Endian
         return unmarshalUnsigned8(data[3])
               + (unmarshalUnsigned8(data[2]) << 8)
               + (unmarshalUnsigned8(data[1]) << 16)
               + ((long) unmarshalUnsigned8(data[0]) << 24);
      }
   }

   /**
    * Unmarshal signed 64 bit from byte array
    *
    * @param data Containing 64 bit value
    * @param order Big / Little Endian byte order
    * @return Value of signed 64 bit
    */
   public static final long unmarshalSigned64(byte[] data, ByteOrder order) {
      if (data.length < 8) {
         throw new IllegalArgumentException("data must contain at least 8 bytes for signed 64bit");
      }

      ByteBuffer b = ByteBuffer.wrap(data);
      b.order(order);

      return b.getLong();
   }

   /**
    * Unmarshal IEEE 754 32 bit from byte array
    *
    * @param data Containing 32 bit value
    * @param order Big / Little Endian byte order
    * @return Value of IEEE 754 32 bit
    */
   public static final float unmarshalIEEE754_32(byte[] data, ByteOrder order) {
      return unmarshalIEEE754_32(data, order, AtomicElementSize.ATOMIC_ELEMENT_8BIT); // 8 bit atomic element version of the function
   }

   /**
    * Unmarshal IEEE 754 32 bit from byte array
    *
    * @param data Containing 32 bit value
    * @param order Big / Little Endian byte order
    * @param atomicElementSize Atomic Element size of the architecture.
    * Determines how bytes are ordered
    * @return Value of IEEE 754 32 bit
    */
   public static final float unmarshalIEEE754_32(byte[] data, ByteOrder order, AtomicElementSize atomicElementSize) {
      if (data.length < 4) {
         throw new IllegalArgumentException("data must contain at least 4 bytes for IEEE 754 32bit");
      }

      // Apply byte order depending on atomic element size
      switch (atomicElementSize) {
         case ATOMIC_ELEMENT_16BIT:
            // Order the bytes so they match 8bit operation handling
            swapBytes(data, 0, 1);
            swapBytes(data, 2, 3);
            break;
      }

      // 8bit atomic element size operation
      ByteBuffer b = ByteBuffer.wrap(data);
      b.order(order);

      return b.getFloat();
   }

   /**
    * Unmarshal IEEE 754 64 bit from byte array
    *
    * @param data Containing 64 bit value
    * @param order Big / Little Endian byte order
    * @return Value of IEEE 754 64 bit
    */
   public static final double unmarshalIEEE754_64(byte[] data, ByteOrder order) {
      if (data.length < 8) {
         throw new IllegalArgumentException("data must contain at least 8 bytes for IEEE 754 64bit");
      }

      ByteBuffer b = ByteBuffer.wrap(data);
      b.order(order);

      return b.getDouble();
   }

   /**
    * Create the BCD byte value represented by the integer
    *
    * @param value BCD value as an integer
    * @return Integer value encoded into BCD byte value
    */
   public static int marshalBCDByteValue(int value) {
      // BCD has two values, each represented by 4bit nibble
      if (value < 0 || value > 99) {
         throw new IllegalArgumentException(value + " out of range for BCD byte value");
      }

      int low = value;
      int high = 0;
      if (value > 9) {
         high = value / 10;
         low = value - (high * 10);
      }

      int val = ((high & 0xF) << 4);
      val += (low & 0xF);
      return val;
   }

   /**
    * Get the BCD byte value contained in the integer
    *
    * @param value BCD value as an integer
    * @return Integer value decoded from byte BCD ecoding
    */
   public static int unmarshalBCDByteValue(int value) {
      // BCD has two values, each represented by 4bit nibble
      int val = (value & 0xF);
      val += ((value >> 4) & 0xF) * 10;
      return val;
   }

   /**
    * Swap bytes in a byte array
    *
    * @param buffer
    * @param index1
    * @param index2
    */
   private static void swapBytes(byte[] buffer, int index1, int index2) {
      if (index1 > buffer.length || index2 > buffer.length) {
         return;   // Cant swap, index out of bounds
      }
      // Swap the bytes given the array and index
      byte tmp = buffer[index1];
      buffer[index1] = buffer[index2];
      buffer[index2] = tmp;
   }

}
