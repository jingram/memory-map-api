/*
 * MemoryMap.java
 * 
 * Defines a class used to encode and decode memory mappings
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map;

import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang.ArrayUtils;
import org.rogueware.memory.map.annotation.MapAESEncryption;
import org.rogueware.memory.map.annotation.MapArray;
import org.rogueware.memory.map.annotation.MapBoolean;
import org.rogueware.memory.map.annotation.MapClass;
import org.rogueware.memory.map.annotation.MapConstant;
import org.rogueware.memory.map.annotation.MapDate;
import org.rogueware.memory.map.annotation.MapNumeric;
import org.rogueware.memory.map.annotation.MapOffset;
import org.rogueware.memory.map.annotation.MapString;
import org.rogueware.memory.map.annotation.MapStructure;
import org.rogueware.memory.map.enums.StringType;
import org.rogueware.memory.map.handler.FieldHandlerFactory;
import org.rogueware.memory.map.handler.FieldHandlerInterface;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class MemoryMap {

   private static final int DEFAULT_BUFFER_SIZE = 16 * 1024;    // Adjust if too small / too big as more experience gained with sizing

   private Map<Class, List<MemoryMapCallbackInterface>> callbacks = new HashMap<Class, List<MemoryMapCallbackInterface>>();

   /**
    * Creates a new instance of MemoryMap
    */
   public MemoryMap() {
   }

   /**
    * Register a callback for the specified memory mapped class.
    *
    * @param <T> Class type
    * @param callback Instance implementing the callback
    * @param klass Memory mapped annotated class type the callback is
    * registering
    * @throws MemoryMapException Unable to add the callback
    */
   public synchronized <T> void registerMemoryMapCallbackHandler(MemoryMapCallbackInterface callback, Class<T> klass) throws MemoryMapException {
      // Make sure callback interface is correct type for class
      try {
         Method m = callback.getClass().getMethod("decoded", klass, MemoryMap.class);
         // Ok :)
      } catch (Exception ex) {
         throw new MemoryMapException("MemoryMapCallbackInterface interface instance does not have decode method for class type " + klass.getName());
      }

      // Make sure the class represents a valid Memory mapped class
      MapStructure mapStructure = getClassAnnotation(klass);

      // Add the class if not already present
      List<MemoryMapCallbackInterface> list = null;
      if (null == (list = callbacks.get(klass))) {
         list = new ArrayList<MemoryMapCallbackInterface>();
         callbacks.put(klass, list);
      }

      // Add the callback to the list for the klass if not already present
      for (MemoryMapCallbackInterface mmc : list) {
         if (mmc.equals(callback)) {
            return;   // Already in the list
         }
      }

      list.add(callback);
   }

   /**
    * Encode a mapped structure "class" into a byte message into the given
    * buffer
    *
    * @param <T> Structure type
    * @param structure Instance of the typed T structure to encode into a byte
    * message
    * @param buffer Buffer to encode message into. Result buffer, position set
    * to original position, bytes remaining = size of encoded message
    * @throws MemoryMapException Encoding error
    */
   public <T> void encodeStructure(T structure, ByteBuffer buffer) throws MemoryMapException {
      encode(structure, buffer);
   }

   /**
    * Decode the byte buffer using the callbacks to notify when a class is
    * decoded
    *
    * @param buffer Byte buffer containing message
    * @return the decoded class
    * @return Returns the class that was decoded
    *
    * @throws MemoryMapException Unable to decode the buffer
    */
   public Class decodeBuffer(ByteBuffer buffer) throws MemoryMapException {
      // Try and decode each class until the first class is decoded
      Object o = null;
      int origPos = buffer.position();
      Class decodedClass = null;

      synchronized (this) {
         for (Class klass : callbacks.keySet()) {
            try {
               buffer.position(origPos);
               o = decode(klass, buffer);

               // It worked, use the decoded class
               decodedClass = klass;
               break;
            } catch (Exception ex) {
               o = null;  // Try next one
            }
         }
      }

      // Fire events if class decoded from message
      if (null != o) {
         synchronized (this) {
            List<MemoryMapCallbackInterface> eventList = callbacks.get(decodedClass);
            for (MemoryMapCallbackInterface mmc : eventList) {
               mmc.decoded(o, this);
            }
         }
      } else {
         buffer.position(origPos);
         throw new MemoryMapException("Unable to decode buffer with registered classes");
      }

      return decodedClass;
   }

   /**
    * Encode a mapped structure "class" into a byte message creating the buffer
    *
    * @param <T> The structure type
    * @param structure Instance of the typed T structure to encode into a byte
    * message
    * @return The buffer contaning the byte stream, position 0 and limit set to
    * size of message
    * @throws MemoryMapException Encoding error
    */
   public static <T> ByteBuffer encode(T structure) throws MemoryMapException {
      // Create a set sized buffer
      byte arrBuf[] = new byte[DEFAULT_BUFFER_SIZE];
      Arrays.fill(arrBuf, (byte) 0x00);
      ByteBuffer buffer = ByteBuffer.wrap(arrBuf);

      encode(structure, buffer);

      return buffer;
   }

   /**
    * Encode a mapped structure "class" into a byte message specifying the
    * buffer
    *
    * @param <T> The structure type
    * @param structure Instance of the typed T structure to encode into a byte
    * message
    * @param buffer Buffer to encode message into. Result buffer, position set
    * to orignal position, bytes remaining = size of encoded message
    * @throws MemoryMapException Encoding error
    */
   public static <T> void encode(T structure, ByteBuffer buffer) throws MemoryMapException {
      // Store the original position of buffer
      int origPos = buffer.position();

      // Set the rest of the buffer bytes to 0x00
      int remainingBytes = buffer.capacity() - buffer.position();
      byte[] zero = new byte[remainingBytes];
      Arrays.fill(zero, (byte) 0x00);
      buffer.put(zero);
      buffer.position(origPos);

      MapStructure mapStructure = getClassAnnotation(structure.getClass());

      // Process each field defiend in the structure instance that has a map annotation
      Map<Integer, Field> fields = getMappedClassFields(structure);

      Iterator<Integer> it = fields.keySet().iterator();
      while (it.hasNext()) {
         encodeField(structure, mapStructure, fields.get(it.next()), buffer);
      }

      // check for size override
      if (-1 != mapStructure.size()) {
         if (buffer.position() - origPos > mapStructure.size()) {
            throw new MemoryMapException("Structure " + structure.getClass().getName() + " overran size by " + (buffer.position() - origPos - mapStructure.size()) + " bytes");
         }
         buffer.position(mapStructure.size());
      }

      buffer.flip();
      buffer.position(origPos);

      // Encrypt if required
      encryptBuffer(structure, buffer);
   }

   /**
    * Decode a byte message into a mapped structure "class"
    *
    * @param <T> The class type
    * @param structureClass Class type to decode
    * @param buffer Buffer containing byte message to decode
    * @return An instance of class T containing fields decoded from byte message
    * @throws MemoryMapException Decoding error
    */
   public static <T> T decode(Class<T> structureClass, ByteBuffer buffer) throws MemoryMapException {
      // Decrypt buffer if required
      decrypteBuffer(structureClass, buffer);

      // Create a new instance of T
      T structure = null;
      try {
         structure = structureClass.newInstance();
      } catch (Exception ex) {
         throw new MemoryMapException("Class " + structureClass.getName() + "' unable to instantiate instance. Error: " + ex.getMessage(), ex);
      }

      MapStructure mapStructure = getClassAnnotation(structure.getClass());
      final int origPos = buffer.position();

      // Process each field defiend in the structure instance that has a map annotation
      Map<Integer, Field> fields = getMappedClassFields(structure);

      Iterator<Integer> it = fields.keySet().iterator();
      while (it.hasNext()) {
         decodeField(structure, mapStructure, fields.get(it.next()), buffer);
      }

      // seek to requested size. used when there are unused bytes at the end such as padding
      if (-1 != mapStructure.size()) {
         final int endPosition = origPos + mapStructure.size();
         if (buffer.position() > endPosition) {
            throw new MemoryMapException("Structure " + structure.getClass().getName() + " overran size by " + (buffer.position() - endPosition) + " bytes");
         }
         buffer.position(endPosition);
      }

      return structure;
   }

   private static final int TAG_BIT_LENGTH = 128;
   private static final int TAG_BYTE_LENGTH = TAG_BIT_LENGTH / 8;
   private static final int IV_BYTE_LENGTH = 12;

   private static <T> void encryptBuffer(T structure,  ByteBuffer buffer) throws MemoryMapException {
      SecretKey aesKey = getClassAESEncryptionKey(structure.getClass());
      if (null == aesKey) return;

      int origPos = buffer.position();
      try {
         byte[] iv = new byte[IV_BYTE_LENGTH];
         new SecureRandom().nextBytes(iv);
         Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
         cipher.init(Cipher.ENCRYPT_MODE, aesKey, new GCMParameterSpec(TAG_BIT_LENGTH, iv));

         // 4 bytes int for encrypted data length
         // TAG_BYTE_LENGTH bytes for GCM tag length security header
         ByteBuffer encryptedBuffer = ByteBuffer.allocate(IV_BYTE_LENGTH + 4 + TAG_BYTE_LENGTH + (buffer.limit() - buffer.position()));
         encryptedBuffer.put(iv);
         encryptedBuffer.putInt((buffer.limit() - buffer.position()));
         cipher.doFinal(buffer, encryptedBuffer);
         
         // Copy the encrypted data to the original byte buffer
         encryptedBuffer.flip();

         buffer.limit(buffer.limit() + iv.length + 4 + TAG_BYTE_LENGTH);  // bytes for GCM tag lengh security header
         buffer.position(origPos);
         buffer.put(encryptedBuffer);
         buffer.flip();
         buffer.position(origPos);
      } catch(Exception ex) {
         throw new MemoryMapException("Structure " + structure.getClass().getName() + " unable to AES encrypt data", ex);         
      }
   }

   private static void decrypteBuffer(Class structureClass,  ByteBuffer buffer) throws MemoryMapException {
      SecretKey aesKey = getClassAESEncryptionKey(structureClass);
      if (null == aesKey) return;

      int origPos = buffer.position();
      try {
         byte[] iv = new byte[IV_BYTE_LENGTH];
         buffer.get(iv);
         int dataLen = buffer.getInt();

         Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
         cipher.init(Cipher.DECRYPT_MODE, aesKey, new GCMParameterSpec(TAG_BIT_LENGTH, iv));

         ByteBuffer decryptedBuffer = ByteBuffer.allocate(dataLen);

         int origLimit = buffer.limit();
         buffer.limit(buffer.position() + TAG_BYTE_LENGTH + dataLen); // bytes for GCM tag lengh security header
         cipher.doFinal(buffer, decryptedBuffer);

      
         // Copy the decrypted data to the original byte buffer
         decryptedBuffer.flip();

         buffer.position(origPos);
         buffer.put(decryptedBuffer);

         // Copy any remaining data after the decrypted data as encrypted data larger than  data
         int cpyFromOffset = origPos + dataLen + IV_BYTE_LENGTH + 4 + TAG_BYTE_LENGTH;
         int cpySize = origLimit - cpyFromOffset;
         buffer.limit(origLimit);
         if (cpySize > 0)
            buffer.put(buffer.array(), cpyFromOffset, cpySize);

         buffer.flip();
         buffer.position(origPos);
      } catch(Exception ex) {
         throw new MemoryMapException("Structure " + structureClass.getName() + " unable to AES decrypt data", ex);
      }
   }

   /**
    * Internal method to encode a field in a structure
    *
    * @param <T>
    * @param structure Instance of the structure the field belongs to
    * @param mapStructure MapStructure annotation for the structure instance
    * @param field Field description for encoding
    * @param buffer Buffer to write encoded bytes to
    * @throws MemoryMapException
    */
   private static <T> void encodeField(T structure, MapStructure mapStructure, Field field, ByteBuffer buffer) throws MemoryMapException {
      // Field Annotation Priority
      // 1) MapOffset                        : Apply defined offset to buffer
      // 2) MapArray                         : Check if the field is mapped as an array and setup array handling if required
      //    MapConstant                      : No special handling. Just encode values in constant (Can even change for input vs output different values)
      // 3) MapXXX                           : Validate and Encode to the mapping type

      // 1) Map Offset
      processMapOffsetAnnotation(structure, field, buffer);

      // 2) Arrays
      Object[] fieldValues = null;
      int arraySize = 0;
      if (-1 != (arraySize = processMapArrayAnnotation(structure, field, false))) {
         // Array of values
         // Convert arrays of primitves to arrays of objects
         if (field.getType().getComponentType().isPrimitive()) {
            // Prmitive Array
            fieldValues = primitiveArrayToObjectArray(getFieldValue(field, structure), field);
         } else {
            // Object Array
            fieldValues = (Object[]) getFieldValue(field, structure);

            // Make sure the values are not > or smaller than the array size
            if (fieldValues.length < arraySize) {
               throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " array size " + arraySize + " only has " + fieldValues.length + " elements");
            } else if (fieldValues.length > arraySize) {
               // Limit the values to the array size
               fieldValues = Arrays.copyOf(fieldValues, arraySize);
            }
         }
      } else {
         // Not an array, set single value
         fieldValues = new Object[1];
         fieldValues[0] = getFieldValue(field, structure);
      }

      // 3) Mappings
      FieldHandlerInterface fh = FieldHandlerFactory.getFieldHandler(mapStructure.byteOrder(), field, structure);
      for (Object fieldVal : fieldValues) {
         fh.write(fieldVal, buffer);
      }
   }

   /**
    * Internal method to decode a field in a structure
    *
    * @param <T>
    * @param structure Instance of the structure the field belongs to
    * @param mapStructure MapStructure annotation for the structure instance
    * @param field Field description for decoding
    * @param buffer Buffer containing raw bytes to decode
    * @throws MemoryMapException
    */
   private static <T> void decodeField(T structure, MapStructure mapStructure, Field field, ByteBuffer buffer) throws MemoryMapException {
      // Field Annotation Priority
      // 1) MapOffset                        : Apply defined offset to buffer
      // 2) MapConstant                      : Check if the field is a constant and read the constant values for comparing from buffer
      // 3) MapArray                         : Check if the field is mapped as an array and setup array handling if required (Creates array if required)
      // 4) MapXXX                           : Validate and Decode to the mapping type, compare value to constants if required

      // 1) Map Offset
      processMapOffsetAnnotation(structure, field, buffer);

      // 2) Constants (Returns null if not constant)
      Object[] constants = processMapConstantAnnotation(structure, field);

      // 3) Arrays
      boolean isArray = true;
      int arraySize = 0;

      // Dont create the array field if its a constant
      if (null == constants) {
         arraySize = processMapArrayAnnotation(structure, field, true);   // Create arrays if not already created in structure
      } else {
         arraySize = processMapArrayAnnotation(structure, field, false);  // Possible constant array, dont allocate memory for array
      }
      if (arraySize == -1) {
         isArray = false;
         arraySize = 1;  // Single element, not an array
      }
      Object[] values = new Object[arraySize];

      // 4) Mappings
      FieldHandlerInterface fh = FieldHandlerFactory.getFieldHandler(mapStructure.byteOrder(), field, structure);
      for (int i = 0; i < arraySize; i++) {
         Object fieldVal = fh.read(buffer);

         // If the field is a constant, ensure the read value ='s the value on the field
         if (null != constants) {
            // Constant
            if (!fh.compareConstant(constants[i], fieldVal)) {
               throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " read value does not match constant value (" + fieldVal + " != " + constants[i] + ")");
            }
         } else {
            // Field value: Save the value
            values[i] = fieldVal;
         }
      } // end for

      // Finally write the field value if its not a constant
      if (null == constants) {
         if (isArray) {

            if (field.getType().getComponentType().isPrimitive()) {
               // Primitive array
               Object primArr = objectArrayToPrimitiveArray(values, field);
               setFieldValue(field, structure, primArr);
            } else {
               // Object array
               // Create specific typed array (Cant just set XXX[] to Object[]
               Object newArr = Array.newInstance(field.getType().getComponentType(), values.length);
               System.arraycopy(values, 0, newArr, 0, values.length);
               setFieldValue(field, structure, newArr);
            }
         } else {
            setFieldValue(field, structure, values[0]);
         }
      }
   }

   /**
    * Internal method to check a class if it is annoted correctly
    *
    * @param <T> The class type
    * @param klass Class to check annotation for
    * @return The MapStructure class annotation instance
    * @throws MemoryMapException Class is not annoted correctly
    */
   public static <T> MapStructure getClassAnnotation(Class<T> klass) throws MemoryMapException {
      MapStructure res = null;
      if (null == (res = klass.getAnnotation(MapStructure.class))) {
         throw new MemoryMapException("Class " + klass.getName() + " is not annotated with the MapStructure annotation");
      }

      // Make sure the class has a default constructor (0 constructors means has default)
      try {
         if (klass.getConstructors().length > 0) {
            // Make sure there is a default constructor
            klass.getConstructor();
         }
      } catch (Exception ex) {
         throw new MemoryMapException("Class " + klass.getName() + " does not implement a default constructor");
      }

      return res;
   }

   /**
    * Internal method to get the AES passphrase if required
    *
    * @param <T> The class type
    * @param klass Class to check annotation for
    * @return The key to use for encryption or null to not encrypt
    * @throws MemoryMapException Unable to obtain AES key
    */
   public static <T> SecretKey getClassAESEncryptionKey(Class<T> klass) throws MemoryMapException {
      MapAESEncryption res = null;
      if (null == (res = klass.getAnnotation(MapAESEncryption.class))) {
         return null; // Does  not require encryption
      }

      String passphrase = res.passphrase();
      if (null == passphrase || passphrase.isEmpty()) {
         throw new MemoryMapException("Class " + klass.getName() + " is annotated with the MapAESEncryption annotation without a passphrase");
      }

      // Build the AES key to use for encryption
      try {
         passphrase = passphrase + klass.getName();
         byte[] salt = {0x10, 0x2f, 0x67, 0x19, 0x5d, 0x12, 0x3f, 0x10};
         SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
         KeySpec spec = new PBEKeySpec(passphrase.toCharArray(), salt, 65536, 256);
         
         SecretKey secret = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
         return secret;      
      } catch(Exception ex) {
         throw new MemoryMapException("Class " + klass.getName() + " MapAESEncryption unable to create key");
      }
   }


   /**
    * Internal method to obtain all the annotated mapped fields and validate
    * them
    *
    * @param <T>
    * @param structure Structure to obtain fields from
    * @return Sorted Map by position containing fields
    * @throws MemoryMapException Validation fails
    */
   private static <T> Map<Integer, Field> getMappedClassFields(T structure) throws MemoryMapException {
      Map<Integer, Field> fields = new TreeMap<Integer, Field>();

      // Traverse all inherited classes looking for mapped annotations
      Class klass = structure.getClass();
      while (null != klass) {
         // Get a list of all defined fields and validate
         for (Field field : klass.getDeclaredFields()) {
            String fieldType;
            int annotationCount = 0;
            int position = 0;

            // Get field type
            if (field.getType().isArray()) {
               fieldType = field.getType().getComponentType().getName();
            } else {
               fieldType = field.getType().getName();
            }

            Annotation an;
            if (null != (an = field.getAnnotation(MapBoolean.class))) {
               // MapBoolean
               MapBoolean mb = (MapBoolean) an;
               annotationCount++;
               position = mb.position();

               // Make sure mapping is attached to a boolean primitive type field
               if (!"boolean".equals(fieldType)) {
                  throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " MapBoolean annotation must annotate a boolean primitive field");
               }

               // Make sure an array with MapBoolean does not have incrementOffset set to false
               if (field.isAnnotationPresent(MapArray.class)) {
                  if (mb.incrementOffset() == false) {
                     throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " contains a boolean array with MapBoolean annotation incrementOffset set to false");
                  }
               }
            }

            if (null != (an = field.getAnnotation(MapClass.class))) {
               // MapClass
               MapClass mc = (MapClass) an;
               annotationCount++;
               position = mc.position();

               // Make sure mapping is attached to a non primitive type
               if ((field.getType().isArray() && field.getType().getComponentType().isPrimitive()) || (field.getType().isPrimitive())) {
                  throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " MapClass annotation must annotate a reference field");
               }

               // Make sure a MapClass is not mapped as a constant
               if (field.isAnnotationPresent(MapConstant.class)) {
                  throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " contains a MapClass annotation with an unsupported MapConstant annotation");
               }
            }

            if (null != (an = field.getAnnotation(MapDate.class))) {
               // MapDate
               MapDate md = (MapDate) an;
               annotationCount++;
               position = md.position();

               // Make sure mapping is attached to a Date class
               if (!"java.util.Date".equals(fieldType)) {
                  throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " MapDate annotation must annotate a java.util.Date reference field");
               }
            }

            if (null != (an = field.getAnnotation(MapNumeric.class))) {
               // MapNumeric
               MapNumeric mn = (MapNumeric) an;
               annotationCount++;
               position = mn.position();

               if (!("byte".equals(fieldType) || "short".equals(fieldType) || "int".equals(fieldType)
                     || "long".equals(fieldType) || "float".equals(fieldType) || "double".equals(fieldType))) {
                  throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " contains a MapNumeric annotation for invalid field type " + fieldType);
               }
            }

            if (null != (an = field.getAnnotation(MapString.class))) {
               // MapString
               MapString ms = (MapString) an;
               annotationCount++;
               position = ms.position();

               if (!"java.lang.String".equals(fieldType)) {
                  throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " MapString annotation must annotate a java.lang.String reference field");
               }

               // Limit charsets supported (Add support as required to handler)
               if (!("US-ASCII".equals(ms.charset()) || "UTF-8".equals(ms.charset()) || "UTF-16".equals(ms.charset())
                     || "UTF-16BE".equals(ms.charset()) || "UTF-16LE".equals(ms.charset()))) {
                  throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " MapString annotation charset='" + ms.charset() + "' not supported by framework");
               }

               // Make sure charset is supported by VM
               try {
                  // Throws exception if charset not supported
                  Charset.isSupported(ms.charset());
               } catch (Exception ex) {
                  throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " MapString annotation charset='" + ms.charset() + "' not supported by VM");
               }

               // Cannot have size with null terminated
               if ((StringType.NULL_TERMINATED == ms.type() || StringType.STRING_TERMINATED == ms.type()) && (ms.stringSize() != 0 || ms.stringSizeMember().length() != 0)) {
                  throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " MapString type NULL_TERMINATED and STRING_TERMINATED cannot have size and sizeMember properties");
               }

               if (StringType.STRING_TERMINATED == ms.type() && ms.terminator().length() == 0) {
                  throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " MapString type STRING_TERMINATED must specify terminator property");
               }

               if (StringType.FIXED == ms.type() && ms.stringSize() == 0 && ms.stringSizeMember().length() == 0) {
                  throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " MapString type FIXED must specify stringSize or stringSizeMember property");
               }
            }

            // Cannot have more than one type mapping per field
            if (annotationCount > 1) {
               throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " contains multiple type mapping annotations");
            }

            // Add the field (Make sure there is no duplicate position)
            if (1 == annotationCount) {
               Field posField;
               if (null != (posField = fields.get(position))) {
                  throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " position " + position + " already defined on field " + posField.getName());
               }

               fields.put(position, field);
            }
         } // end for

         klass = klass.getSuperclass();

         if ("java.lang.Object".equals(klass.getName())) {
            klass = null;   // Dont bother with Object
         }
      } // end while

      return fields;
   }

   /**
    * Internal method to process any offset annotations for buffer incrementing
    * / decrementing reletive or absolutre
    *
    * @param <T>
    * @param structure Instance the field belongs to
    * @param field Field definition
    * @param buffer Buffer that offset will be applied to
    * @throws MemoryMapException
    */
   private static <T> void processMapOffsetAnnotation(T structure, Field field, ByteBuffer buffer) throws MemoryMapException {
      MapOffset mapOffset = null;
      if (null == (mapOffset = field.getAnnotation(MapOffset.class))) {
         return;  // No offset defined
      }
      // Sanity
      if (-1 != mapOffset.absolute() && -1 != mapOffset.relative()) {
         throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " annotation MapOffset defined both relative and absolute positions");
      }
      if (-1 == mapOffset.absolute() && -1 == mapOffset.relative()) {
         throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " annotation MapOffset does not define a relative or absolute position");
      }

      if (-1 != mapOffset.absolute()) {
         // Absolute offset
         if (mapOffset.absolute() < 0) {
            throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " annotation MapOffset cannot have -ve absolute position into buffer");
         }
         if (mapOffset.absolute() > buffer.limit()) {
            throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " annotation MapOffset absolute position > buffer size");
         }

         buffer.position(mapOffset.absolute());
      } else {
         // Relative offset
         int newPos = buffer.position() + mapOffset.relative();  // Create new position from current position

         if (newPos < 0) {
            throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " annotation MapOffset relative position buffer under run");
         }
         if (newPos > buffer.limit()) {
            throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " annotation MapOffset relative position buffer over run");
         }

         buffer.position(newPos);
      }
   }

   /**
    * Find and process a MapArray annotation on a mapped field or an array field
    * if no MapArray annotation Allocated memory to array if required
    *
    * @param <T>
    * @param structure Instance the field belongs to
    * @param field Field definition
    * @param createArrays True to allocate memory for array if required, false
    * to not allocate memory
    * @return Size of array if present, -1 if not an array
    * @throws MemoryMapException
    */
   private static <T> int processMapArrayAnnotation(T structure, Field field, boolean createArrays) throws MemoryMapException {
      int size = -1;  // Not an array

      MapArray mapArray = field.getAnnotation(MapArray.class);

      // Sanity
      if (null != mapArray && !field.getType().isArray()) {
         throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " annotated as array using MapArray but field type not defined as an array type");
      }

      // If there is no annotation, check for default array type field
      if (null != mapArray) {
         // Annotated, get required size using annotations         
         if (0 != mapArray.sizeMember().length() && mapArray.size() != 0) {
            throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " annotation MapArray defined both size and sizeMember");
         }

         if (mapArray.sizeMember().length() > 0) {
            size = getSizeMemberFieldValue(structure, mapArray.sizeMember());

            if (size < 0) {
               throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " annotation MapArray size must be >= 0 for sizeMember='" + mapArray.sizeMember() + "'");
            }
         } else {
            // Size
            if (mapArray.size() < 0) {
               throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " annotation MapArray size must be >= 0");
            }

            size = mapArray.size();
         }

         // Create the array if required
         if (createArrays) {
            try {
               setFieldValue(field, structure, Array.newInstance(field.getType().getComponentType(), size));
            } catch (Exception ex) {
               throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " annotation MapArray unable to allocate array size='" + size + "' for field", ex);
            }
         }
      } else if (field.getType().isArray()) {
         // NOT ANNOTATED, but is an array, get size from defined array size
         try {
            Object[] arr = (Object[]) getFieldValue(field, structure);
            size = arr.length;

            // Note: Field element is already allocated the correct array size
         } catch (Exception ex) {
            throw new MemoryMapException("Class " + structure.getClass().getName() + " field " + field.getName() + " annotation MapArray unable to get array value from field='" + field.getName() + "'. Error: " + ex.getMessage(), ex);
         }
      }

      return size;
   }

   /**
    * Internal method to determine if a field is a constant and return the
    * constant values back for processing
    *
    * @param <T>
    * @param structure Instance of the object
    * @param field Field to look for MapConstant annotation on
    * @return Constant value
    * @throws MemoryMapException
    */
   private static <T> Object[] processMapConstantAnnotation(T structure, Field field) throws MemoryMapException {
      MapConstant mapConstant = null;

      // MapConstant Annotation
      if (null == (mapConstant = field.getAnnotation(MapConstant.class))) {
         return null;  // Not a constant value
      }

      // We have a constant, get all the values for the constant
      Object val = getFieldValue(field, structure);
      Object[] res = null;

      // If the result is an array, return as an array
      if (field.getType().isArray()) {
         if (field.getType().getComponentType().isPrimitive()) {
            // Primitive Array
            res = primitiveArrayToObjectArray(val, field);
         } else {
            // Object Array
            res = (Object[]) val;
         }
      } else {
         res = new Object[1];
         res[0] = val;
      }

      return res;
   }

   /**
    * Internal method to set the field value using set or setter
    *
    * @param field Description of field to set value for
    * @param obj Instance to set value for field on
    * @param value Field value to set
    * @throws MemoryMapException Unable to set the value
    */
   private static void setFieldValue(Field field, Object obj, Object value) throws MemoryMapException {
      try {
         // 1. Use 'public' setter incase of custom logic (Prioritised)
         // 2. If no setter, set the field value

         // Look for a setter that takes the correct type
         String fieldName = field.getName();
         fieldName = fieldName.toUpperCase().charAt(0) + fieldName.substring(1);
         Method setter = null;
         try {
            setter = obj.getClass().getMethod("set" + fieldName, field.getType());
         } catch (Exception ex) {
            setter = null;
         }

         if (null != setter && Modifier.isPublic(setter.getModifiers())) {
            setter.invoke(obj, value);
         } else {
            // Set the field overriding access
            field.setAccessible(true);
            field.set(obj, value);
         }
      } catch (Exception ex) {
         throw new MemoryMapException("Class " + obj.getClass().getName() + " field='" + field.getName() + "' unable to set value", ex);
      }
   }

   /**
    * Internal method to return the field value using get or getter
    *
    * @param field Description of field to obtain value for
    * @param obj Instance to obtain field value from
    * @return Value
    * @throws MemoryMapException Unable to obtain value
    */
   private static Object getFieldValue(Field field, Object obj) throws MemoryMapException {
      try {
         // 1. Use 'public' getter incase of custom logic (Prioritised)
         // 2. If no getter, get the field value

         // Look for a getter         
         String fieldName = field.getName();
         fieldName = fieldName.toUpperCase().charAt(0) + fieldName.substring(1);
         Method getter = null;
         try {
            getter = obj.getClass().getMethod("get" + fieldName);
         } catch (Exception ex) {
            // Look for is getter
            try {
               getter = obj.getClass().getMethod("is" + fieldName);
            } catch (Exception ex2) {
               getter = null;
            }
         }

         if (null != getter && Modifier.isPublic(getter.getModifiers()) &&
             getter.getReturnType().getName().equals(field.getType().getName())  ) {   // Must return the same type, or read field value
            return getter.invoke(obj);
         } else {
            // Get the field overriding access
            field.setAccessible(true);
            return field.get(obj);
         }
      } catch (NullPointerException ex) {
         throw new MemoryMapException("Class " + obj.getClass().getName() + " field='" + field.getName() + "' value is NULL");
      } catch (Exception ex) {
         throw new MemoryMapException("Class " + obj.getClass().getName() + " field='" + field.getName() + "' unable to obtain field value. Error: " + ex.getMessage(), ex);
      }
   }

   /**
    * Internal method to obtain the fixed size using annotation paramters
    *
    *
    * @param structure Field name containing the size parameter
    * @param memberName Field name containing the size parameter
    * @return The fixed size specified by the field
    *
    * @throws MemoryMapException Error reading value
    */
   public static final int getSizeMemberFieldValue(Object structure, String memberName) throws MemoryMapException {
      // Get the size from a member attribute
      Field member = null;
      Class klass = structure.getClass();
      while (null == member && null != klass) {
         try {
            member = klass.getDeclaredField(memberName);
         } catch (Exception ex) {
            member = null;
         }

         klass = klass.getSuperclass();

         if ("java.lang.Object".equals(klass.getName())) {
            klass = null;   // Dont bother with Object
         }
      } // end while

      if (null == member) {
         throw new MemoryMapException("Unable to find size member field '" + memberName + "'");
      }


      Object o = MemoryMap.getFieldValue(member, structure);
      // Try convert the string value to a number
      if (o instanceof String) {
         String siz = (String)o; 
         if (null == o || siz.isEmpty()) throw new MemoryMapException("Size member field that is a string cannot be null or an empty string"); 
         
         try {
            int val = Integer.parseInt(siz.trim());
            return val;
         } catch(Exception ex) {
            throw new MemoryMapException("Size member field string value is not a valid number value");
         }
      }

      // Assume the value is a primitive
      if (!(o instanceof Number)) {
         throw new MemoryMapException("Size member field must be of type primitive byte, short, int, long, float, double");
      }
      Number val = (Number) o;

      return val.intValue();
   }

   /**
    * Internal method to convert an array of primitives to objects
    *
    * @param primArr Primitive array
    * @param field Field describing attributes of array
    * @return Object array
    * @throws MemoryMapException Unable to convert
    */
   private static Object[] primitiveArrayToObjectArray(Object primArr, Field field) throws MemoryMapException {
      Object[] res = null;

      String primName = field.getType().getComponentType().getName();

      if ("boolean".equals(primName)) {
         res = ArrayUtils.toObject((boolean[]) primArr);
      } else if ("byte".equals(primName)) {
         res = ArrayUtils.toObject((byte[]) primArr);
      } else if ("short".equals(primName)) {
         res = ArrayUtils.toObject((short[]) primArr);
      } else if ("int".equals(primName)) {
         res = ArrayUtils.toObject((int[]) primArr);
      } else if ("long".equals(primName)) {
         res = ArrayUtils.toObject((long[]) primArr);
      } else if ("float".equals(primName)) {
         res = ArrayUtils.toObject((float[]) primArr);
      } else if ("double".equals(primName)) {
         res = ArrayUtils.toObject((double[]) primArr);
      } else {
         throw new MemoryMapException("Unable to convert primtive type array to object array for type " + primName);
      }

      return res;
   }

   /**
    * Internal method to convert and object array into a primtive array
    *
    * @param objectArr Object array
    * @param field Field describing attributes of array
    * @return Object representing primitive array
    * @throws MemoryMapException Unable to convert
    */
   private static Object objectArrayToPrimitiveArray(Object[] objectArr, Field field) throws MemoryMapException {
      Object res = null;

      String primName = field.getType().getComponentType().getName();

      if ("boolean".equals(primName)) {
         Boolean[] tArr = new Boolean[objectArr.length];
         System.arraycopy(objectArr, 0, tArr, 0, objectArr.length);
         res = (Object) ArrayUtils.toPrimitive(tArr);
      } else if ("byte".equals(primName)) {
         Byte[] tArr = new Byte[objectArr.length];
         System.arraycopy(objectArr, 0, tArr, 0, objectArr.length);
         res = (Object) ArrayUtils.toPrimitive(tArr);
      } else if ("short".equals(primName)) {
         Short[] tArr = new Short[objectArr.length];
         System.arraycopy(objectArr, 0, tArr, 0, objectArr.length);
         res = (Object) ArrayUtils.toPrimitive(tArr);
      } else if ("int".equals(primName)) {
         Integer[] tArr = new Integer[objectArr.length];
         System.arraycopy(objectArr, 0, tArr, 0, objectArr.length);
         res = (Object) ArrayUtils.toPrimitive(tArr);
      } else if ("long".equals(primName)) {
         Long[] tArr = new Long[objectArr.length];
         System.arraycopy(objectArr, 0, tArr, 0, objectArr.length);
         res = (Object) ArrayUtils.toPrimitive(tArr);
      } else if ("float".equals(primName)) {
         Float[] tArr = new Float[objectArr.length];
         System.arraycopy(objectArr, 0, tArr, 0, objectArr.length);
         res = (Object) ArrayUtils.toPrimitive(tArr);
      } else if ("double".equals(primName)) {
         Double[] tArr = new Double[objectArr.length];
         System.arraycopy(objectArr, 0, tArr, 0, objectArr.length);
         res = (Object) ArrayUtils.toPrimitive(tArr);
      } else {
         throw new MemoryMapException("Unable to convert object array to primitive type array for type " + primName);
      }

      return res;
   }
}
