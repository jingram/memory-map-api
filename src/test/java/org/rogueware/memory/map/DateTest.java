/*
 * DateTest.java
 *
 * Defines a class used used to test memory mapper
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map;

import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import junit.framework.*;
import org.rogueware.memory.map.annotation.MapArray;
import org.rogueware.memory.map.annotation.MapConstant;
import org.rogueware.memory.map.annotation.MapDate;
import org.rogueware.memory.map.annotation.MapStructure;
import org.rogueware.memory.map.enums.AtomicElementSize;
import org.rogueware.memory.map.enums.ByteOrder;
import org.rogueware.memory.map.enums.DateEpoch;
import org.rogueware.memory.map.enums.DateSize;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class DateTest extends TestCase {
   // Test Class (Init items opposite to test case setting)

   /**
    * Creates a new instance of XMLConfigurationTest
    *
    * @param testName
    */
   public DateTest(String testName) {
      super(testName);
   }

   private void checkByte(int b, int check) {
      if (b != check) {
         fail("Byte check failed. Expecting byte 0x" + Integer.toString(check, 16) + " got byte 0x" + Integer.toString(b, 16));
      }
   }

   public void testEncodeJavaEndian() {
      Date d1 = null;
      try {
         d1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S Z").parse("2009-04-01 19:42:12.523 -0000");   // 1 Apr 2009 23h42:12:523 UTC = 1238614932523 Java milliseconds since epoch
      } catch (Exception ex) {
         fail("Unable to setup dates for test");
      }

      // Big Endian
      try {
         MyDateJAVABEStructure structure = new MyDateJAVABEStructure();
         structure.date = d1;

         ByteBuffer result = MemoryMap.encode(structure);

         // Check the byte buffer result to ensure encoding
         checkByte(result.get(), 0x0);  // 64 bit signed JAVA long date value
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x1);
         checkByte(result.get(), 0x20);

         checkByte(result.get(), 0x63);
         checkByte(result.get(), 0x33);
         checkByte(result.get(), -0x4);
         checkByte(result.get(), 0x2b);

         if (result.remaining() > 0) {
            fail("Too many bytes remaining in result");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Date encode test failed with exception: " + ex.getMessage());
      }

      // Little Endian
      try {
         MyDateJAVALEStructure structure = new MyDateJAVALEStructure();
         structure.date = d1;

         ByteBuffer result = MemoryMap.encode(structure);

         // Check the byte buffer result to ensure encoding
         checkByte(result.get(), 0x2b);
         checkByte(result.get(), -0x4);
         checkByte(result.get(), 0x33);
         checkByte(result.get(), 0x63);

         checkByte(result.get(), 0x20);
         checkByte(result.get(), 0x1);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);  // 64 bit signed JAVA long date value

         if (result.remaining() > 0) {
            fail("Too many bytes remaining in result");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Date encode test failed with exception: " + ex.getMessage());
      }
   }

   public void testDecodeJavaEndian() {
      // Big Endian
      try {
         MyDateJAVABEStructure structure = null;

         byte srcArr[] = {0x0, 0x0, 0x01, 0x20, 0x63, 0x33, -0x4, 0x2b};
         ByteBuffer src = ByteBuffer.wrap(srcArr);

         structure = MemoryMap.decode(MyDateJAVABEStructure.class, src);

         if (1238614932523L != structure.date.getTime()) {
            fail("date supposed to be 1238614932523 milliseconds");
         }

         if (src.remaining() > 0) {
            fail("Too many bytes remaining in source buffer");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Boolean decode test failed with exception: " + ex.getMessage());
      }

      // Little Endian
      try {
         MyDateJAVALEStructure structure = null;

         byte srcArr[] = {0x2b, -0x4, 0x33, 0x63, 0x20, 0x1, 0x0, 0x0};
         ByteBuffer src = ByteBuffer.wrap(srcArr);

         structure = MemoryMap.decode(MyDateJAVALEStructure.class, src);

         if (1238614932523L != structure.date.getTime()) {
            fail("date supposed to be 1238614932523 milliseconds");
         }

         if (src.remaining() > 0) {
            fail("Too many bytes remaining in source buffer");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Date decode test failed with exception: " + ex.getMessage());
      }

   }

   public void testEncodeUnixEndian() {
      Date d1 = null;
      try {
         d1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S Z").parse("2009-03-05 23:42:12.00 -0000");   // 5 Mar 2009 23h42:12 UTC = 1236296532 unix seconds since epoch 
      } catch (Exception ex) {
         fail("Unable to setup dates for test");
      }

      // Big Endian
      try {
         MyDateUnixBEStructure structure = new MyDateUnixBEStructure();
         structure.date = d1;

         ByteBuffer result = MemoryMap.encode(structure);

         // Check the byte buffer result to ensure encoding
         checkByte(result.get(), 0x0);  // Constant value 0 for epoch
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);

         checkByte(result.get(), 0x49);
         checkByte(result.get(), -0x50);
         checkByte(result.get(), 0x63);
         checkByte(result.get(), 0x54);

         if (result.remaining() > 0) {
            fail("Too many bytes remaining in result");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Date encode test failed with exception: " + ex.getMessage());
      }

      // Little Endian
      try {
         MyDateUnixLEStructure structure = new MyDateUnixLEStructure();
         structure.date = d1;

         ByteBuffer result = MemoryMap.encode(structure);

         // Check the byte buffer result to ensure encoding
         checkByte(result.get(), 0x0);  // Constant value 0 for epoch
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);

         checkByte(result.get(), 0x54);
         checkByte(result.get(), 0x63);
         checkByte(result.get(), -0x50);
         checkByte(result.get(), 0x49);

         if (result.remaining() > 0) {
            fail("Too many bytes remaining in result");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Date encode test failed with exception: " + ex.getMessage());
      }

   }

   public void testDecodeUnixEndian() {
      // Big Endian
      try {
         MyDateUnixBEStructure structure = null;

         byte srcArr[] = {0x0, 0x0, 0x0, 0x0, 0x49, -0x50, 0x63, 0x54};
         ByteBuffer src = ByteBuffer.wrap(srcArr);

         structure = MemoryMap.decode(MyDateUnixBEStructure.class, src);

         if (0 != structure.dateConst.getTime()) {
            fail("dateConst supposed to be 0");
         }
         if (1236296532000L != structure.date.getTime()) {
            fail("date supposed to be 1236296532 seconds");
         }

         if (src.remaining() > 0) {
            fail("Too many bytes remaining in source buffer");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Boolean decode test failed with exception: " + ex.getMessage());
      }

      // Little Endian
      try {
         MyDateUnixLEStructure structure = null;

         byte srcArr[] = {0x0, 0x0, 0x0, 0x0, 0x54, 0x63, -0x50, 0x49};
         ByteBuffer src = ByteBuffer.wrap(srcArr);

         structure = MemoryMap.decode(MyDateUnixLEStructure.class, src);

         if (0 != structure.dateConst.getTime()) {
            fail("dateConst supposed to be 0");
         }
         if (1236296532000L != structure.date.getTime()) {
            fail("date supposed to be 1236296532 seconds");
         }

         if (src.remaining() > 0) {
            fail("Too many bytes remaining in source buffer");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Date decode test failed with exception: " + ex.getMessage());
      }
   }

   public void testEncodeBCDDayTime() {
      Calendar now = Calendar.getInstance();
      Date d1 = null;
      try {
         // Force the year and month to current month. They are ignored in the date format
         d1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S Z").parse(now.get(Calendar.YEAR) + "-" + (now.get(Calendar.MONTH) + 1) + "-13 16:56:41.00 +0200");   //Will be represented as local time
      } catch (Exception ex) {
         fail("Unable to setup dates for test");
      }

      // Big Endian
      try {
         MyDateBCDDayTimeStructure structure = new MyDateBCDDayTimeStructure();
         structure.date = d1;

         ByteBuffer result = MemoryMap.encode(structure);

         // Check the byte buffer result to ensure encoding
         checkByte(result.get(), 0x13);  // Constant value 0 for epoch
         checkByte(result.get(), 0x16);
         checkByte(result.get(), 0x56);
         checkByte(result.get(), 0x41);

         if (result.remaining() > 0) {
            fail("Too many bytes remaining in result");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Date encode test failed with exception: " + ex.getMessage());
      }
   }

   public void testDencodeBCDDayTime() {
      // Big Endian
      SimpleDateFormat bcdCmp = new SimpleDateFormat("dd HH:mm:ss.S Z");
      try {
         Calendar now = Calendar.getInstance();
         Date d1 = null;
         try {
            // Force the year and month to current month. They are ignored in the date format
            d1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S Z").parse(now.get(Calendar.YEAR) + "-" + (now.get(Calendar.MONTH) + 1) + "-13 16:56:41.00 +0200");   //Will be represented as local time
         } catch (Exception ex) {
            fail("Unable to setup dates for test");
         }

         MyDateBCDDayTimeStructure structure = null;

         byte srcArr[] = {0x13, 0x16, 0x56, 0x41};
         ByteBuffer src = ByteBuffer.wrap(srcArr);

         structure = MemoryMap.decode(MyDateBCDDayTimeStructure.class, src);

         if (!bcdCmp.format(d1).equals(bcdCmp.format(structure.date))) {
            fail("date supposed to be " + d1.getTime() + " seconds");
         }

         if (src.remaining() > 0) {
            fail("Too many bytes remaining in source buffer");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Date decode test failed with exception: " + ex.getMessage());
      }
   }

   public void testEncodeArray() {
      Date d1 = null;
      Date d2 = null;
      Date d3 = null;
      try {
         d1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S Z").parse("2009-03-05 23:42:12.00 -0000");   // 5 Mar 2009 23h42:12 UTC = 1236296532 unix seconds since epoch
         d2 = new Date(0);  // Epoch
         d3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S Z").parse("2009-03-05 23:42:12.00 -0000");   // 5 Mar 2009 23h42:12 UTC = 1236296532 unix seconds since epoch
      } catch (Exception ex) {
         fail("Unable to setup dates for test");
      }

      // Big Endian
      try {
         MyDateArrayStructure structure = new MyDateArrayStructure();
         Date dArr[] = {d1, d2, d3};
         structure.date = dArr;

         ByteBuffer result = MemoryMap.encode(structure);

         // Check the byte buffer result to ensure encoding
         checkByte(result.get(), 0x49);
         checkByte(result.get(), -0x50);
         checkByte(result.get(), 0x63);
         checkByte(result.get(), 0x54);

         checkByte(result.get(), 0x0);  // Constant value 0 for epoch
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);

         checkByte(result.get(), 0x49);
         checkByte(result.get(), -0x50);
         checkByte(result.get(), 0x63);
         checkByte(result.get(), 0x54);

         if (result.remaining() > 0) {
            fail("Too many bytes remaining in result");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Date encode test failed with exception: " + ex.getMessage());
      }
   }

   public void testDecodeArray() {
      // Big Endian
      try {
         MyDateArrayStructure structure = null;

         byte srcArr[] = {0x49, -0x50, 0x63, 0x54, 0x0, 0x0, 0x0, 0x0, 0x49, -0x50, 0x63, 0x54};
         ByteBuffer src = ByteBuffer.wrap(srcArr);

         structure = MemoryMap.decode(MyDateArrayStructure.class, src);

         if (1236296532000L != structure.date[0].getTime()) {
            fail("date[0] supposed to be 1236296532 seconds");
         }
         if (0 != structure.date[1].getTime()) {
            fail("date[1] supposed to be 0");
         }
         if (1236296532000L != structure.date[2].getTime()) {
            fail("date[2] supposed to be 1236296532 seconds");
         }

         if (src.remaining() > 0) {
            fail("Too many bytes remaining in source buffer");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Boolean decode test failed with exception: " + ex.getMessage());
      }
   }

   public void testError() {
      // Error 1
      try {
         MyDateMapErrorStructure structure = new MyDateMapErrorStructure();

         ByteBuffer result = MemoryMap.encode(structure);

         fail("Map error");
      } catch (MemoryMapException ex) {
         if (!ex.getMessage().contains("MapDate annotation must annotate a java.util.Date reference field")) {
            fail("Expecting exception message containing 'MapDate annotation must annotate a java.util.Date reference field'");
         }
      }
   }
}

@MapStructure(byteOrder = ByteOrder.BIG_ENDIAN)
class MyDateUnixBEStructure {

   @MapConstant
   @MapDate(epoch = DateEpoch.UNIX, size = DateSize.UNSIGNED_32BIT, position = 0)
   public Date dateConst = new Date(0);  // Epoch Constant

   @MapDate(epoch = DateEpoch.UNIX, size = DateSize.UNSIGNED_32BIT, position = 1)
   public Date date;
}

@MapStructure(byteOrder = ByteOrder.LITTLE_ENDIAN)
class MyDateUnixLEStructure {

   @MapConstant
   @MapDate(epoch = DateEpoch.UNIX, size = DateSize.UNSIGNED_32BIT, position = 0)
   public Date dateConst = new Date(0);  // Epoch Constant

   @MapDate(epoch = DateEpoch.UNIX, size = DateSize.UNSIGNED_32BIT, position = 1)
   public Date date;
}

@MapStructure
class MyDateJAVABEStructure {

   @MapDate(epoch = DateEpoch.JAVA, size = DateSize.SIGNED_64BIT, position = 0)
   public Date date;
}

@MapStructure
class MyDateJAVALEStructure {

   @MapDate(epoch = DateEpoch.JAVA, size = DateSize.SIGNED_64BIT, byteOrder = ByteOrder.LITTLE_ENDIAN, position = 0)
   public Date date;
}

@MapStructure()
class MyDateBCDDayTimeStructure {

   @MapDate(epoch = DateEpoch.BCD_DAY_TIME, size = DateSize.UNSIGNED_32BIT, byteOrder = ByteOrder.BIG_ENDIAN, atomicElementSize = AtomicElementSize.ATOMIC_ELEMENT_16BIT, position = 1)
   public Date date;
}

@MapStructure
class MyDateArrayStructure {

   @MapDate(epoch = DateEpoch.UNIX, size = DateSize.UNSIGNED_32BIT, position = 0)
   @MapArray(size = 3)
   public Date date[];
}

@MapStructure
class MyDateMapErrorStructure {

   // Cannot set increment = false for array mapping of boolean.
   // Arrray mappings are limited to 1 byte at a time
   @MapDate(epoch = DateEpoch.UNIX, position = 0)
   public int boolErr;
}
