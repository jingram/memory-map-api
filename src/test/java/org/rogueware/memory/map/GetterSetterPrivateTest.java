/*
 * GetterSetterPrivateTest.java
 *
 * Defines a class used used to test memory mapper
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map;

import java.nio.ByteBuffer;
import junit.framework.*;
import org.rogueware.memory.map.annotation.MapNumeric;
import org.rogueware.memory.map.annotation.MapString;
import org.rogueware.memory.map.annotation.MapStructure;
import org.rogueware.memory.map.enums.NumericType;

/**
 *
 * @author Johnathan Ingram
 */
public class GetterSetterPrivateTest extends TestCase {

   /**
    * Creates a new instance of XMLConfigurationTest
    *
    * @param testName
    */
   public GetterSetterPrivateTest(String testName) {
      super(testName);
   }

   private void checkByte(int b, int check) {
      if (b != check) {
         fail("Byte check failed. Expecting byte 0x" + Integer.toString(check, 16) + " got byte 0x" + Integer.toString(b, 16));
      }
   }

   public void testEncodePrivateFields() {
      PrivateFields structure = new PrivateFields();

      try {
         ByteBuffer result = MemoryMap.encode(structure);

         // Check the byte buffer result to ensure encoding
         checkByte(result.get(), 'a');
         checkByte(result.get(), 'b');
         checkByte(result.get(), 'c');
         checkByte(result.get(), 0x0);

         if (result.remaining() > 0) {
            fail("Too many bytes remaining in result");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Class encode test failed with exception: " + ex.getMessage());
      }
   }

   public void testEncodeProtectedFields() {
      ProtectedFields structure = new ProtectedFields();

      try {
         ByteBuffer result = MemoryMap.encode(structure);

         // Check the byte buffer result to ensure encoding
         checkByte(result.get(), 'c');
         checkByte(result.get(), 'b');
         checkByte(result.get(), 'a');
         checkByte(result.get(), 0x0);

         if (result.remaining() > 0) {
            fail("Too many bytes remaining in result");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Class encode test failed with exception: " + ex.getMessage());
      }
   }

   public void testDecodeUsingSetterLogic() {
      SetterOverride structure = null;

      try {
         // Value from wire is hello, but value decoded will be PRE-hello-SUF due to setter logic
         byte srcArr[] = {'h', 'e', 'l', 'l', 'o', 0x0};
         ByteBuffer src = ByteBuffer.wrap(srcArr);

         // This will use the specified setter with custom logic
         structure = MemoryMap.decode(SetterOverride.class, src);

         if (null == structure) {
            fail("structure not supposed to be null");
         }
         if (!"PRE-hello-SUF".equals(structure.val)) {
            fail("val supposed to be 'PRE-hello-SUF'");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Class decode test failed with exception: " + ex.getMessage());
      }
   }

   
  public void testEncodeOverrideGetter() {
      GetterOverride structure = new GetterOverride();

      try {
         structure.setVal("hello");
         ByteBuffer result = MemoryMap.encode(structure);

         // Check the byte buffer result to ensure encoding
         checkByte(result.get(), 'W');
         checkByte(result.get(), 'I');
         checkByte(result.get(), 'R');
         checkByte(result.get(), 'E');
         checkByte(result.get(), 'D');
         checkByte(result.get(), '-');
         checkByte(result.get(), 'h');
         checkByte(result.get(), 'e');
         checkByte(result.get(), 'l');
         checkByte(result.get(), 'l');
         checkByte(result.get(), 'o');
         checkByte(result.get(), 0x0);

         if (result.remaining() > 0) {
            fail("Too many bytes remaining in result");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Class encode test failed with exception: " + ex.getMessage());
      }
   }   
}


@MapStructure
class PrivateFields {

   @MapString(position = 0)
   private String strVal = "abc";   // Default NULL_TERMINATED, US-ASCII
}

@MapStructure
class ProtectedFields {

   @MapString(position = 0)
   protected String strVal = "cba";   // Default NULL_TERMINATED, US-ASCII
}

@MapStructure
class SetterOverride {

   @MapString(position = 0)
   public String val;   // Default NULL_TERMINATED, US-ASCII

   public void setVal(String val) {
      this.val = "PRE-" + val + "-SUF";
   }
}

@MapStructure
class GetterOverride {

   @MapString(position = 0)
   private String val;   // Default NULL_TERMINATED, US-ASCII

   public void setVal(String val) {
      this.val = val;
   }

   public String getVal() {
      return "WIRED-" + val;
   }
}
