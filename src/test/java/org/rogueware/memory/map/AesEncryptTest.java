/*
 * AesEncryptTest.java
 *
 * Defines a class used used to test memory mapper
 * 
 * Copyright 2023 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map;

import java.nio.ByteBuffer;
import java.util.Date;
import junit.framework.*;

import org.rogueware.memory.map.annotation.MapAESEncryption;
import org.rogueware.memory.map.annotation.MapBoolean;
import org.rogueware.memory.map.annotation.MapClass;
import org.rogueware.memory.map.annotation.MapDate;
import org.rogueware.memory.map.annotation.MapString;
import org.rogueware.memory.map.annotation.MapStructure;
import org.rogueware.memory.map.enums.DateEpoch;
import org.rogueware.memory.map.enums.DateSize;
import org.rogueware.memory.map.enums.StringType;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class AesEncryptTest extends TestCase {
   // Test Class (Init items opposite to test case setting)

   /**
    * Creates a new instance of XMLConfigurationTest
    *
    * @param testName
    */
   public AesEncryptTest(String testName) {
      super(testName);
   }

   public void testEncodeClass() {
      try {
         MyAesClassStructure structure = new MyAesClassStructure();
         ByteBuffer result = MemoryMap.encode(structure);

         MyAesClassStructure cmpStructure = MemoryMap.decode(MyAesClassStructure.class, result);
         assertEquals(structure.dt.getTime() /1000, cmpStructure.dt.getTime() /1000);
         assertEquals(structure.string, cmpStructure.string);
         assertEquals(structure.klass.bool, cmpStructure.klass.bool);
         assertEquals(structure.klass.string, cmpStructure.klass.string);
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("String encode test failed with exception: " + ex.getMessage());
      }
   }

   public void testEncodeMultipleClasses() {
      try {
         MyAesClassStructure structure = new MyAesClassStructure();
         ByteBuffer result = MemoryMap.encode(structure);

         MyAesClassStructure structure2 = new MyAesClassStructure();
         structure2.string = "I am #2";
         structure2.klass.bool = false;
         result.position(result.limit());  // Encode after last
         result.limit(result.capacity());  // Allow writing more data to buffer
         MemoryMap.encode(structure2, result);

         MyAesClassStructure structure3 = new MyAesClassStructure();
         structure3.string = "I am #3";
         structure3.klass.string = "The world goes round and round and round";
         result.position(result.limit());  // Encode after last
         result.limit(result.capacity());  // Allow writing more data to buffer
         MemoryMap.encode(structure3, result);


         result.position(0);  // Decode from first class

         MyAesClassStructure cmpStructure = MemoryMap.decode(MyAesClassStructure.class, result);
         assertEquals(structure.dt.getTime() /1000, cmpStructure.dt.getTime() /1000);
         assertEquals(structure.string, cmpStructure.string);
         assertEquals(structure.klass.bool, cmpStructure.klass.bool);
         assertEquals(structure.klass.string, cmpStructure.klass.string);

         MyAesClassStructure cmpStructure2 = MemoryMap.decode(MyAesClassStructure.class, result);
         assertEquals(structure2.dt.getTime() /1000, cmpStructure2.dt.getTime() /1000);
         assertEquals(structure2.string, cmpStructure2.string);
         assertEquals(structure2.klass.bool, cmpStructure2.klass.bool);
         assertEquals(structure2.klass.string, cmpStructure2.klass.string);

         MyAesClassStructure cmpStructure3 = MemoryMap.decode(MyAesClassStructure.class, result);
         assertEquals(structure3.dt.getTime() /1000, cmpStructure3.dt.getTime() /1000);
         assertEquals(structure3.string, cmpStructure3.string);
         assertEquals(structure3.klass.bool, cmpStructure3.klass.bool);
         assertEquals(structure3.klass.string, cmpStructure3.klass.string);
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("String encode test failed with exception: " + ex.getMessage());
      }
   }
      

}


@MapStructure
class MyAesClassMemberStructure {
   @MapBoolean(position = 0)
   public boolean bool = true;   

   @MapString(type = StringType.STRING_TERMINATED, position = 1)  
   public String string = " .. as bob";
}

@MapStructure
@MapAESEncryption(passphrase = "This is the passphrase")
class MyAesClassStructure {

   @MapDate(epoch = DateEpoch.UNIX, size = DateSize.UNSIGNED_32BIT, position = 0)
   public Date dt = new Date(423705600l);

   @MapString(type = StringType.NULL_TERMINATED, position = 1)  
   public String string = "Hello World";

   @MapClass(className = "org.rogueware.memory.map.MyAesClassMemberStructure", position = 2)
   public MyAesClassMemberStructure klass = new MyAesClassMemberStructure();
}
