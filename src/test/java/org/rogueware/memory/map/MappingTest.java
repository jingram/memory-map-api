/*
 * MappingTest.java
 *
 * Defines a class used used to test memory mapper
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map;

import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import junit.framework.TestCase;
import org.rogueware.memory.map.annotation.MapArray;
import org.rogueware.memory.map.annotation.MapBoolean;
import org.rogueware.memory.map.annotation.MapClass;
import org.rogueware.memory.map.annotation.MapConstant;
import org.rogueware.memory.map.annotation.MapDate;
import org.rogueware.memory.map.annotation.MapNumeric;
import org.rogueware.memory.map.annotation.MapOffset;
import org.rogueware.memory.map.annotation.MapString;
import org.rogueware.memory.map.annotation.MapStructure;
import org.rogueware.memory.map.enums.BooleanBit;
import org.rogueware.memory.map.enums.ByteOrder;
import org.rogueware.memory.map.enums.DateEpoch;
import org.rogueware.memory.map.enums.DateSize;
import org.rogueware.memory.map.enums.NumericType;
import org.rogueware.memory.map.enums.StringType;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class MappingTest extends TestCase {

   /**
    * Creates a new instance of MappingTest
    *
    * @param testName
    */
   public MappingTest(String testName) {
      super(testName);
   }

   // Callback classes
   class OneClassback implements MemoryMapCallbackInterface<MyStructureOne> {

      @Override
      public void decoded(MyStructureOne structure, MemoryMap memoryMap) {
         System.out.println("Got a MyStructureOne");

         if (!"ONE".equals(structure.magic)) {
            fail("MyStructureOne.magic incorrect");
         }
         if (2 != structure.arraySize) {
            fail("MyStructureOne.arraySize incorrect");
         }
         if (196821732981L != structure.dates[0].getTime()) {
            fail("MyStructureOne.dates[0] incorrect");
         }
         if (946684799999L != structure.dates[1].getTime()) {
            fail("MyStructureOne.dates[1] incorrect");
         }
         if (!"© Thank you for decoding this string.".equals(structure.suffix)) {
            fail("MyStructureOne.suffix incorrect");
         }
      }
   }

   class TwoClassback implements MemoryMapCallbackInterface<MyStructureTwo> {

      @Override
      public void decoded(MyStructureTwo structure, MemoryMap memoryMap) {
         System.out.println("Got a MyStructureTwo");

         if (!"TWO".equals(structure.magic)) {
            fail("MyStructureTwo.magic incorrect");
         }
         if (!structure.isFlag5) {
            fail("MyStructureTwo.isFlag5 incorrect");
         }
         if (structure.isFlag7) {
            fail("MyStructureTwo.isFlag7 incorrect");
         }
         if (structure.positions.length != 3) {
            fail("MyStructureTwo.positions incorrect");
         }
         if (1000 != structure.positions[0].threePos[0] || 2000 != structure.positions[0].threePos[1] || 3000 != structure.positions[0].threePos[2]) {
            fail("MyStructureTwo.positions[0] incorrect");
         }
         if (-30100 != structure.positions[1].threePos[0] || 30100 != structure.positions[1].threePos[1] || -6534 != structure.positions[1].threePos[2]) {
            fail("MyStructureTwo.positions[1] incorrect");
         }
         if (0 != structure.positions[2].threePos[0] || Short.MAX_VALUE != structure.positions[2].threePos[1] || Short.MIN_VALUE != structure.positions[2].threePos[2]) {
            fail("MyStructureTwo.positions[2] incorrect");
         }
         if (34678123.876543215f != structure.number) {
            fail("MyStructureTwo.number incorrect");
         }
      }
   }

   public void testMappings() {
      short sa1[] = {1000, 2000, 3000}, sa2[] = {-30100, 30100, -6534}, sa3[] = {0, Short.MAX_VALUE, Short.MIN_VALUE};

      try {
         ByteBuffer bufferOne = ByteBuffer.allocate(4096);
         ByteBuffer bufferTwo = ByteBuffer.allocate(4096);

         MyStructureOne one = new MyStructureOne();
         one.arraySize = 2;
         one.dates = new Date[3];  // Will only encode 2 of the dates
         one.dates[0] = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S Z").parse("1976-03-28 02:42:12.981 +0200");
         one.dates[1] = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S Z").parse("1999-12-31 23:59:59.999 +0000");
         one.dates[2] = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S Z").parse("2009-04-16 06:47:33.000 +0000");
         one.suffix = "© Thank you for decoding this string.";  // Will add terminator to this

         MyStructureTwo two = new MyStructureTwo();
         two.isFlag5 = true;
         two.isFlag7 = false;
         two.positions = new MyStructureSmall[3];
         two.positions[0] = new MyStructureSmall();
         two.positions[0].threePos = sa1;
         two.positions[1] = new MyStructureSmall();
         two.positions[1].threePos = sa2;
         two.positions[2] = new MyStructureSmall();
         two.positions[2].threePos = sa3;
         two.number = 34678123.876543215f;

         MemoryMap mm = new MemoryMap();

         // Test encoding
         mm.encodeStructure(one, bufferOne);
         mm.encodeStructure(two, bufferTwo);

         // Test decoding
         mm.registerMemoryMapCallbackHandler(new OneClassback(), MyStructureOne.class);
         mm.registerMemoryMapCallbackHandler(new TwoClassback(), MyStructureTwo.class);

         bufferOne.rewind();
         bufferTwo.rewind();

         mm.decodeBuffer(bufferTwo);
         mm.decodeBuffer(bufferOne);
      } catch (Exception ex) {
         fail("Mapping test failed with exception error: " + ex.getMessage());
      }
   }

   public void testMappingsNeg() {
      MemoryMap mm = new MemoryMap();

      // Wrong class for incorrect template callback interface
      try {
         mm.registerMemoryMapCallbackHandler(new OneClassback(), MyStructureTwo.class);
         fail("Added incorrect callback interface template for specified class");
      } catch (MemoryMapException ex) {
         // Ok
      }
   }
}

@MapStructure
class MyStructureOne {

   @MapConstant                                                 // WARNING: Multiple classes should have some form of constant to force correct mapping
   @MapString(type = StringType.FIXED, stringSize = 3, position = 0)
   public final String magic = "ONE";          // US-ASCII

   @MapNumeric(type = NumericType.UNSIGNED_16BIT, position = 1)
   public long arraySize;

   @MapArray(sizeMember = "arraySize")
   @MapDate(epoch = DateEpoch.JAVA, size = DateSize.SIGNED_64BIT, position = 2)
   public Date dates[];

   @MapString(type = StringType.STRING_TERMINATED, charset = "UTF-8", terminator = "©2009≠", position = 3)
   public String suffix;
}

@MapStructure
class MyStructureTwo {

   @MapOffset(absolute = 5)                                       // Skip first 5 bytes. They are not required in mapping
   @MapConstant                                                 // WARNING: Multiple classes should have some form of constant to force correct mapping
   @MapString(type = StringType.FIXED, charset = "UTF-16", stringSize = 3, position = 0)
   public String magic = "TWO";          // UTF-16

   @MapOffset(relative = 2)                                       // Skip 2 more bytes
   @MapBoolean(bit = BooleanBit.BIT_5, incrementOffset = false, position = 1)
   public boolean isFlag5;

   @MapBoolean(bit = BooleanBit.BIT_7, position = 2)
   public boolean isFlag7;

   @MapArray(size = 3)
   @MapClass(className = "org.rogueware.memory.map.MyStructureSmall", position = 3)
   public MyStructureSmall positions[];

   @MapNumeric(type = NumericType.IEEE_754_32BIT, position = 4)
   public float number;
}

@MapStructure(byteOrder = ByteOrder.LITTLE_ENDIAN)
class MyStructureSmall {

   @MapArray(size = 3)
   @MapNumeric(type = NumericType.SIGNED_16BIT, position = 0)
   public short threePos[];
}
