/*
 * StringTest.java
 *
 * Defines a class used used to test memory mapper
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map;

import java.nio.ByteBuffer;
import java.util.Date;
import junit.framework.*;
import org.rogueware.memory.map.annotation.MapConstant;
import org.rogueware.memory.map.annotation.MapString;
import org.rogueware.memory.map.annotation.MapStructure;
import org.rogueware.memory.map.enums.StringType;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class StringTest extends TestCase {
   // Test Class (Init items opposite to test case setting)

   /**
    * Creates a new instance of XMLConfigurationTest
    *
    * @param testName
    */
   public StringTest(String testName) {
      super(testName);
   }

   private void checkByte(int b, int check) {
      if (b != check) {
         fail("Byte check failed. Expecting byte 0x" + Integer.toString(check, 16) + " got byte 0x" + Integer.toString(b, 16));
      }
   }

   public void testEncodeFixedJava() {
      try {
         MyStringFixedStructure structure = new MyStringFixedStructure();

         ByteBuffer result = MemoryMap.encode(structure);

         // Check the byte buffer result to ensure encoding
         checkByte(result.get(), '1');  // US-ASCII string
         checkByte(result.get(), '2');
         checkByte(result.get(), '3');
         checkByte(result.get(), '4');
         checkByte(result.get(), '5');
         checkByte(result.get(), ' '); // Padding
         checkByte(result.get(), ' ');
         checkByte(result.get(), ' ');

         checkByte(result.get(), -0x3e); // UTF-8 string
         checkByte(result.get(), -0x57);
         checkByte(result.get(), '2');
         checkByte(result.get(), '0');
         checkByte(result.get(), '0');
         checkByte(result.get(), '9');
         checkByte(result.get(), -0x1e);
         checkByte(result.get(), -0x77);
         checkByte(result.get(), -0x60);

         if (result.remaining() > 0) {
            fail("Too many bytes remaining in result");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("String encode test failed with exception: " + ex.getMessage());
      }
   }

   public void testDecodeFixedJava() {
      try {
         MyStringFixedStructure structure = null;

         byte srcArr[] = {'1', '2', '3', '4', '5', ' ', ' ', ' ', -0x3e, -0x57, '2', '0', '0', '9', -0x1e, -0x77, -0x60};
         ByteBuffer src = ByteBuffer.wrap(srcArr);

         structure = MemoryMap.decode(MyStringFixedStructure.class, src);

         if (!"12345   ".equals(structure.strConst)) {
            fail("strConst supposed to be '12345   '");
         }
         if (!"©2009≠".equals(structure.strConst2)) {
            fail("strConst2 supposed to be '©2009≠'");
         }

         if (src.remaining() > 0) {
            fail("Too many bytes remaining in source buffer");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("String decode test failed with exception: " + ex.getMessage());
      }
   }

   public void testDecode2FixedJava() {
      try {
         // Testing size member being a string
         MyStringFixedStructure2 structure = null;

         byte srcArr[] = {'1', '2', '3', '4', '5', ' ', ' ', ' ', '6', ' ', -0x3e, -0x57, '2', '0', '0', '9', -0x1e, -0x77, -0x60};
         ByteBuffer src = ByteBuffer.wrap(srcArr);

         structure = MemoryMap.decode(MyStringFixedStructure2.class, src);

         if (!"12345   ".equals(structure.strConst)) {
            fail("strConst supposed to be '12345   '");
         }
         if (!"6 ".equals(structure.strSize)) {
            fail("strSize supposed to be '6 '");
         }
         if (!"©2009≠".equals(structure.strConst2)) {
            fail("strConst2 supposed to be '©2009≠'");
         }

         if (src.remaining() > 0) {
            fail("Too many bytes remaining in source buffer");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("String decode test failed with exception: " + ex.getMessage());
      }
   }

   public void testEncodeNullJava() {
      try {
         MyStringNULLStructure structure = new MyStringNULLStructure();

         ByteBuffer result = MemoryMap.encode(structure);

         // Check the byte buffer result to ensure encoding
         checkByte(result.get(), 'H');  // US-ASCII string
         checkByte(result.get(), 'e');
         checkByte(result.get(), 'l');
         checkByte(result.get(), 'l');
         checkByte(result.get(), 'o');
         checkByte(result.get(), '!');
         checkByte(result.get(), 0x0);  // NULL Terminated

         checkByte(result.get(), -0x2);  // UTF-16 string  (BE LE start byte)
         checkByte(result.get(), -0x1);  //                (BE LE start byte)
         checkByte(result.get(), 0x6);
         checkByte(result.get(), 0x41);
         checkByte(result.get(), 0x0);  // NULL Terminated
         checkByte(result.get(), 0x0);  // NULL Terminated

         checkByte(result.get(), -0x3e); // UTF-8 string
         checkByte(result.get(), -0x57);
         checkByte(result.get(), '2');
         checkByte(result.get(), '0');
         checkByte(result.get(), '0');
         checkByte(result.get(), '9');
         checkByte(result.get(), 0x0);  // // NULL Terminated

         if (result.remaining() > 0) {
            fail("Too many bytes remaining in result");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("String encode test failed with exception: " + ex.getMessage());
      }
   }

   public void testDecodeNullJava() {
      try {
         MyStringNULLStructure structure = null;

         byte srcArr[] = {'H', 'e', 'l', 'l', 'o', '!', 0x0, -0x2, -0x1, 0x6, 0x41, 0x0, 0x0, -0x3e, -0x57, '2', '0', '0', '9', 0x0};
         ByteBuffer src = ByteBuffer.wrap(srcArr);

         structure = MemoryMap.decode(MyStringNULLStructure.class, src);

         if (!"Hello!".equals(structure.strConst)) {
            fail("strConst supposed to be 'Hello!'");
         }
         if (!"ف".equals(structure.strConst2)) {
            fail("strConst supposed to be 'ف'");
         }
         if (!"©2009".equals(structure.strConst3)) {
            fail("strConst3 supposed to be '©2009'");
         }

         if (src.remaining() > 0) {
            fail("Too many bytes remaining in source buffer");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("String decode test failed with exception: " + ex.getMessage());
      }
   }

   public void testEncodeTermJava() {
      try {
         MyStringTermStructure structure = new MyStringTermStructure();

         ByteBuffer result = MemoryMap.encode(structure);

         // Check the byte buffer result to ensure encoding
         checkByte(result.get(), 'W');  // US-ASCII string
         checkByte(result.get(), 'o');
         checkByte(result.get(), 'r');
         checkByte(result.get(), 'l');
         checkByte(result.get(), 'd');
         checkByte(result.get(), '\r');
         checkByte(result.get(), '\n');

         checkByte(result.get(), -0x2); // UTF-16 string  (BE LE start byte)
         checkByte(result.get(), -0x1); //                (BE LE start byte)
         checkByte(result.get(), 0x0);  // UTF-16 string with arabic character in terminator
         checkByte(result.get(), 'B');
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 'i');
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 'g');
         checkByte(result.get(), 0x0); // Start of terminator
         checkByte(result.get(), '\r');
         checkByte(result.get(), 0x6);
         checkByte(result.get(), 0x41);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), '\n');

         checkByte(result.get(), -0x3e); // UTF-8 string
         checkByte(result.get(), -0x57);
         checkByte(result.get(), '2');
         checkByte(result.get(), '0');
         checkByte(result.get(), '0');
         checkByte(result.get(), '9');
         checkByte(result.get(), ':'); // Terminator

         if (result.remaining() > 0) {
            fail("Too many bytes remaining in result");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("String encode test failed with exception: " + ex.getMessage());
      }
   }

   public void testDecodeTermJava() {
      try {
         MyStringTermStructure structure = null;

         byte srcArr[] = {'W', 'o', 'r', 'l', 'd', '\r', '\n', -0x2, -0x1, 0x0, 'B', 0x0, 'i', 0x0, 'g', 0x0, '\r', 0x6, 0x41, 0x0, '\n', -0x3e, -0x57, '2', '0', '0', '9', ':'};
         ByteBuffer src = ByteBuffer.wrap(srcArr);

         structure = MemoryMap.decode(MyStringTermStructure.class, src);

         if (!"World".equals(structure.strConst)) {
            fail("strConst supposed to be 'World'");
         }
         if (!"Big".equals(structure.strConst2)) {
            fail("strConst2 supposed to be 'Big'");
         }
         if (!"©2009".equals(structure.strConst3)) {
            fail("strConst3 supposed to be '©2009'");
         }

         if (src.remaining() > 0) {
            fail("Too many bytes remaining in source buffer");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("String decode test failed with exception: " + ex.getMessage());
      }
   }

   public void testError() {
      // Error 1
      try {
         MyStringMapErrorStructure structure = new MyStringMapErrorStructure();

         ByteBuffer result = MemoryMap.encode(structure);

         fail("Map error");
      } catch (MemoryMapException ex) {
         if (!ex.getMessage().contains("MapString annotation must annotate a java.lang.String reference field")) {
            fail("Expecting exception message containing 'MapString annotation must annotate a java.lang.String reference field'");
         }
      }

      // Error 2
      try {
         MyStringMapError2Structure structure = new MyStringMapError2Structure();

         ByteBuffer result = MemoryMap.encode(structure);

         fail("Map error NULL_TERMINATED");
      } catch (MemoryMapException ex) {
         if (!ex.getMessage().contains("MapString type NULL_TERMINATED and STRING_TERMINATED cannot have size and sizeMember properties")) {
            fail("Expecting exception message containing 'MapString type NULL_TERMINATED and STRING_TERMINATED cannot have size and sizeMember properties'");
         }
      }

      // Error 3
      try {
         MyStringMapCharsetErrorStructure structure = new MyStringMapCharsetErrorStructure();

         ByteBuffer result = MemoryMap.encode(structure);

         fail("Map error Charset");
      } catch (MemoryMapException ex) {
         if (!ex.getMessage().contains("MapString annotation charset=")) {
            fail("Expecting exception message containing 'MapString annotation charset='");
         }
      }

   }
}

@MapStructure
class MyStringNULLStructure {

   @MapConstant
   @MapString(position = 0)
   protected String strConst = "Hello!";        // Default NULL_TERMINATED, US-ASCII

   @MapConstant
   @MapString(charset = "UTF-16", position = 1)
   public String strConst2 = "ف";            // Default NULL_TERMINATED (1 arabic letter)

   @MapConstant
   @MapString(charset = "UTF-8", position = 2)
   public String strConst3 = "©2009";         // UTF-8

}

@MapStructure
class MyStringTermStructure {

   @MapConstant
   @MapString(type = StringType.STRING_TERMINATED, terminator = "\r\n", position = 0)
   public String strConst = "World";          // US-ASCII

   @MapConstant
   @MapString(type = StringType.STRING_TERMINATED, charset = "UTF-16", terminator = "\rف\n", position = 1)
   public String strConst2 = "Big";           // UTF-16

   @MapConstant
   @MapString(type = StringType.STRING_TERMINATED, charset = "UTF-8", terminator = ":", position = 2)
   public String strConst3 = "©2009";         // UTF-8
}

@MapStructure
class MyStringFixedStructure {

   @MapString(type = StringType.FIXED, stringSize = 8, position = 0)
   public String strConst = "12345";          // US-ASCII   // Will need to pad

   @MapString(type = StringType.FIXED, charset = "UTF-8", stringSizeMember = "strSize", position = 1)
   public String strConst2 = "©2009≠";        // UTF-8

   // This is not mapped, just used for string size test. It could be mapped :)
   public int strSize = 6;
}

@MapStructure
class MyStringFixedStructure2 {

   @MapString(type = StringType.FIXED, stringSize = 8, position = 0)
   public String strConst = "12345";          // US-ASCII   // Will need to pad

   @MapString(type = StringType.FIXED, charset = "UTF-8", stringSize =  2, position = 1)
   public String strSize = "6 ";  // Test mapping size from a string

   @MapString(type = StringType.FIXED, charset = "UTF-8", stringSizeMember = "strSize", position = 2)
   public String strConst2 = "©2009≠";        // UTF-8
}


@MapStructure
class MyStringMapErrorStructure {

   @MapString(position = 0)
   private Date stringErr;
}

@MapStructure
class MyStringMapError2Structure {

   @MapString(type = StringType.NULL_TERMINATED, stringSize = 100, position = 0)  // Cannot have size specified with NULL_TERMINATED
   private String stringErr;
}

@MapStructure
class MyStringMapCharsetErrorStructure {

   @MapString(charset = "bla bla", position = 0)
   private String stringErr;
}
