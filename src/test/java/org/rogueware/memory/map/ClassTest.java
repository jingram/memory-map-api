/*
 * ClassTest.java
 *
 * Defines a class used used to test memory mapper
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map;

import java.nio.ByteBuffer;
import junit.framework.*;
import org.rogueware.memory.map.annotation.MapArray;
import org.rogueware.memory.map.annotation.MapBoolean;
import org.rogueware.memory.map.annotation.MapClass;
import org.rogueware.memory.map.annotation.MapConstant;
import org.rogueware.memory.map.annotation.MapOffset;
import org.rogueware.memory.map.annotation.MapStructure;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class ClassTest extends TestCase {

   /**
    * Creates a new instance of XMLConfigurationTest
    *
    * @param testName
    */
   public ClassTest(String testName) {
      super(testName);
   }

   private void checkByte(int b, int check) {
      if (b != check) {
         fail("Byte check failed. Expecting byte 0x" + Integer.toString(check, 16) + " got byte 0x" + Integer.toString(b, 16));
      }
   }

   public void testEncode() {
      MyClassMemberStructure member = new MyClassMemberStructure();
      member.bool = true;
      MyClassMemberStructure member2 = new MyClassMemberStructure();
      member2.bool = false;

      MyClassStructure structure = new MyClassStructure();
      structure.klass = member;
      structure.klassArr = new MyClassMemberStructure[3];
      structure.klassArr[0] = member;
      structure.klassArr[1] = member2;
      structure.klassArr[2] = member;

      try {
         ByteBuffer result = MemoryMap.encode(structure);

         // Check the byte buffer result to ensure encoding
         checkByte(result.get(), 0x0);  // Only start at absolute pos 2
         checkByte(result.get(), 0x0);  // Only start at absolute pos 2
         checkByte(result.get(), -0x1);  // Boolean value of first member
         checkByte(result.get(), -0x1);  // klassArr[0] true
         checkByte(result.get(), 0x0);  // klassArr[1] false
         checkByte(result.get(), -0x1);  // klassArr[2] true

         if (result.remaining() > 0) {
            fail("Too many bytes remaining in result");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Class encode test failed with exception: " + ex.getMessage());
      }
   }

   public void testDecode() {
      MyClassStructure structure = null;

      try {
         byte srcArr[] = {0x0, 0x0, 0x1, 0x1, 0x0, 0x1};
         ByteBuffer src = ByteBuffer.wrap(srcArr);

         structure = MemoryMap.decode(MyClassStructure.class, src);

         if (null == structure.klass) {
            fail("klass not supposed to be null");
         }
         if (!structure.klass.bool) {
            fail("klass.bool supposed to be true");
         }
         if (null == structure.klassArr) {
            fail("klassArr not supposed to be null");
         }
         if (!structure.klassArr[0].bool) {
            fail("klassArr[0].bool supposed to be true");
         }
         if (structure.klassArr[1].bool) {
            fail("klassArr[1].bool supposed to be false");
         }
         if (!structure.klassArr[2].bool) {
            fail("klassArr[2].bool supposed to be true");
         }

         if (src.remaining() > 0) {
            fail("Too many bytes remaining in source buffer");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Class decode test failed with exception: " + ex.getMessage());
      }
   }

   public void testError() {
      // Error 1
      try {
         MyClassConstErrorStructure structure = new MyClassConstErrorStructure();

         ByteBuffer result = MemoryMap.encode(structure);

         fail("Constant error not generated");
      } catch (MemoryMapException ex) {
         if (!ex.getMessage().contains("contains a MapClass annotation with an unsupported MapConstant annotation")) {
            fail("Expecting exception message containing 'contains a MapClass annotation with an unsupported MapConstant annotation'");
         }
      }

      // Error 2
      try {
         MyClassNoMapStructureErrorStructure structure = new MyClassNoMapStructureErrorStructure();
         structure.err = new Integer(100);

         ByteBuffer result = MemoryMap.encode(structure);

         fail("MapStructure error not generated");
      } catch (MemoryMapException ex) {
         if (!ex.getMessage().contains("is not annotated with the MapStructure annotation")) {
            fail("Expecting exception message containing 'is not annotated with the MapStructure annotation'");
         }
      }

      // Error 3
      try {
         MyClassMapErrorStructure structure = new MyClassMapErrorStructure();
         structure.err = new Integer(100);

         ByteBuffer result = MemoryMap.encode(structure);

         fail("Map type error not generated");
      } catch (MemoryMapException ex) {
         if (!ex.getMessage().contains("MapClass annotation must annotate a reference field")) {
            fail("Expecting exception message containing 'MapClass annotation must annotate a reference field'");
         }
      }

   }

}

@MapStructure
class MyClassStructure {

   @MapOffset(absolute = 2)

   @MapClass(className = "org.rogueware.memory.map.MyClassMemberStructure", position = 0)
   public MyClassMemberStructure klass;

   @MapArray(size = 3)
   @MapClass(className = "org.rogueware.memory.map.MyClassMemberStructure", position = 1)
   public MyClassMemberStructure klassArr[];

   // Class types must be annotated with @MapClass or they will automatically be ignored!!!
   public String ignoreMe;
}

@MapStructure
class MyClassMemberStructure {

   @MapBoolean(position = 0)
   public boolean bool;
}

// Error classes
@MapStructure
class MyClassConstErrorStructure {

   // A class cannot be used as a constant
   @MapConstant
   @MapClass(className = "org.rogueware.memory.map.MyClassMemberStructure", position = 0)
   public MyClassMemberStructure klass;
}

@MapStructure
class MyClassNoMapStructureErrorStructure {

   // Mapped class must implement @MapStructure interface
   @MapClass(className = "java.lang.Integer", position = 0)
   public Integer err;
}

@MapStructure
class MyClassMapErrorStructure {

   // Mapped class must implement @MapStructure interface
   @MapClass(className = "java.lang.Integer", position = 0)
   public int err;
}
