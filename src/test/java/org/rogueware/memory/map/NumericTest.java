/*
 * NumericTest.java
 *
 * Defines a class used used to test memory mapper
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map;

import java.nio.ByteBuffer;
import junit.framework.*;
import org.rogueware.memory.map.annotation.MapArray;
import org.rogueware.memory.map.annotation.MapConstant;
import org.rogueware.memory.map.annotation.MapNumeric;
import org.rogueware.memory.map.annotation.MapStructure;
import org.rogueware.memory.map.enums.AtomicElementSize;
import org.rogueware.memory.map.enums.ByteOrder;
import org.rogueware.memory.map.enums.NumericType;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class NumericTest extends TestCase {
   // Test Class (Init items opposite to test case setting)

   /**
    * Creates a new instance of XMLConfigurationTest
    *
    * @param testName
    */
   public NumericTest(String testName) {
      super(testName);
   }

   private void checkByte(int b, int check) {
      if (b != check) {
         fail("Byte check failed. Expecting byte 0x" + Integer.toString(check, 16) + " got byte 0x" + Integer.toString(b, 16));
      }
   }

   public void testEncodeEndian() {
      // Big Endian
      try {
         MyNumericBEStructure structure = new MyNumericBEStructure();
         structure.inCastVal = 19000;
         structure.setLongVal(Long.MIN_VALUE);

         ByteBuffer result = MemoryMap.encode(structure);

         // Check the byte buffer result to ensure encoding
         checkByte(result.get(), 0x7f);  // 16 bit constant
         checkByte(result.get(), -0x1);

         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x4a);
         checkByte(result.get(), 0x38);

         checkByte(result.get(), -0x80);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);

         checkByte(result.get(), 0x0A);  // Unsigned 16 bit int
         checkByte(result.get(), 0x0B);
         checkByte(result.get(), 0x0C);
         checkByte(result.get(), 0x0D);

         checkByte(result.get(), 0x0A);  // Unsigned 16 bit int (atomic element size 16)
         checkByte(result.get(), 0x0B);
         checkByte(result.get(), 0x0C);
         checkByte(result.get(), 0x0D);

         if (result.remaining() > 0) {
            fail("Too many bytes remaining in result");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Date encode test failed with exception: " + ex.getMessage());
      }

      // Little Endian
      try {
         MyNumericLEStructure structure = new MyNumericLEStructure();
         structure.inCastVal = 19000;
         structure.longVal = Long.MIN_VALUE;

         ByteBuffer result = MemoryMap.encode(structure);

         // Check the byte buffer result to ensure encoding
         checkByte(result.get(), -0x1);
         checkByte(result.get(), 0x7f);  // 16 bit constant

         checkByte(result.get(), 0x38);
         checkByte(result.get(), 0x4a);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);

         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), -0x80);

         checkByte(result.get(), 0x0D);  // Unsigned 16 bit int
         checkByte(result.get(), 0x0C);
         checkByte(result.get(), 0x0B);
         checkByte(result.get(), 0x0A);

         checkByte(result.get(), 0x0C);  // Unsigned 16 bit int (atomic element size 16)
         checkByte(result.get(), 0x0D);
         checkByte(result.get(), 0x0A);
         checkByte(result.get(), 0x0B);

         if (result.remaining() > 0) {
            fail("Too many bytes remaining in result");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Date encode test failed with exception: " + ex.getMessage());
      }
   }

   public void testDecodeEndian() {

      // Big Endian
      try {
         MyNumericBEStructure structure = null;

         byte srcArr[] = {0x7f, -0x1, 0x0, 0x0, 0x4a, 0x38, -0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0A, 0x0B, 0x0C, 0x0D, 0x0A, 0x0B, 0x0C, 0x0D};
         ByteBuffer src = ByteBuffer.wrap(srcArr);

         structure = MemoryMap.decode(MyNumericBEStructure.class, src);

         if (Short.MAX_VALUE != structure.shortConst) {
            fail("shortConst supposed to be Short.MAX_VALUE");
         }
         if (19000 != structure.inCastVal) {
            fail("inCastVal supposed to be 19000");
         }
         if (Long.MIN_VALUE != structure.getLongVal()) {
            fail("getLongVal() supposed to be Long.MAX_VALUE");
         }
         if (structure.classicValue != 0x0A0B0C0D) {
            fail("classicValue supposed to be 0x0A0B0C0D");
         }
         if (structure.classicValueAtomic16 != 0x0A0B0C0D) {
            fail("classicValueAtomic16 supposed to be 0x0A0B0C0D");
         }

         if (src.remaining() > 0) {
            fail("Too many bytes remaining in source buffer");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Boolean decode test failed with exception: " + ex.getMessage());
      }

      // Little Endian
      try {
         MyNumericLEStructure structure = null;

         byte srcArr[] = {-0x1, 0x7f, 0x38, 0x4a, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, -0x80, 0x0D, 0x0C, 0x0B, 0x0A, 0x0C, 0x0D, 0x0A, 0x0B};
         ByteBuffer src = ByteBuffer.wrap(srcArr);

         structure = MemoryMap.decode(MyNumericLEStructure.class, src);

         if (Short.MAX_VALUE != structure.shortConst) {
            fail("shortConst supposed to be Short.MAX_VALUE");
         }
         if (19000 != structure.inCastVal) {
            fail("inCastVal supposed to be 19000");
         }
         if (Long.MIN_VALUE != structure.longVal) {
            fail("longVal supposed to be Long.MAX_VALUE");
         }
         if (structure.classicValue != 0x0A0B0C0D) {
            fail("classicValue supposed to be 0x0A0B0C0D");
         }
         if (structure.classicValueAtomic16 != 0x0A0B0C0D) {
            fail("classicValueAtomic16 supposed to be 0x0A0B0C0D");
         }

         if (src.remaining() > 0) {
            fail("Too many bytes remaining in source buffer");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Boolean decode test failed with exception: " + ex.getMessage());
      }
   }

   public void testEncodeArray() {
      // Big Endian
      try {
         MyNumericArrayStructure structure = new MyNumericArrayStructure();
         int intArr[] = {Integer.MAX_VALUE, 0, Integer.MIN_VALUE};
         structure.setInts(intArr);

         ByteBuffer result = MemoryMap.encode(structure);

         // Check the byte buffer result to ensure encoding
         checkByte(result.get(), 0x7f);
         checkByte(result.get(), -0x1);
         checkByte(result.get(), -0x1);
         checkByte(result.get(), -0x1);

         checkByte(result.get(), 0x0);  // Constant value 0 for epoch
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);

         checkByte(result.get(), -0x80);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);
         checkByte(result.get(), 0x0);

         if (result.remaining() > 0) {
            fail("Too many bytes remaining in result");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Date encode test failed with exception: " + ex.getMessage());
      }
   }

   public void testDecodeArray() {
      // Big Endian
      try {
         MyNumericArrayStructure structure = null;

         byte srcArr[] = {0x7f, -0x1, -0x1, -0x1, 0x0, 0x0, 0x0, 0x0, -0x80, -0x0, 0x0, 0x0};
         ByteBuffer src = ByteBuffer.wrap(srcArr);

         structure = MemoryMap.decode(MyNumericArrayStructure.class, src);

         if (Integer.MAX_VALUE != structure.getInts()[0]) {
            fail("getInts()[0] supposed to be Integer.MAX");
         }
         if (0 != structure.getInts()[1]) {
            fail("getInts()[1] supposed to be 0");
         }
         if (Integer.MIN_VALUE != structure.getInts()[2]) {
            fail("getInts()[2] supposed to be Integer.MIN");
         }

         if (src.remaining() > 0) {
            fail("Too many bytes remaining in source buffer");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Boolean decode test failed with exception: " + ex.getMessage());
      }
   }

   public void testError() {
      // Error 1
      try {
         MyNumericMapErrorStructure structure = new MyNumericMapErrorStructure();

         ByteBuffer result = MemoryMap.encode(structure);

         fail("Map error");
      } catch (MemoryMapException ex) {
         if (!ex.getMessage().contains("contains a MapNumeric annotation for invalid field type")) {
            fail("Expecting exception message containing 'contains a MapNumeric annotation for invalid field type'");
         }
      }
   }
}

@MapStructure(byteOrder = ByteOrder.BIG_ENDIAN)
class MyNumericBEStructure {

   @MapConstant
   @MapNumeric(type = NumericType.SIGNED_16BIT, position = 0)
   public short shortConst = Short.MAX_VALUE;

   @MapNumeric(type = NumericType.SIGNED_32BIT, position = 1)
   public short inCastVal;                                  // Cast signed 32 bit into 16 bit

   @MapNumeric(type = NumericType.SIGNED_64BIT, position = 2)
   private long longVal;                                    // Less than public, must have getter / setter

   @MapNumeric(type = NumericType.UNSIGNED_32BIT, position = 3)
   public long classicValue = 0x0A0B0C0D;

   @MapNumeric(type = NumericType.UNSIGNED_32BIT, atomicElementSize = AtomicElementSize.ATOMIC_ELEMENT_16BIT, position = 4)
   public long classicValueAtomic16 = 0x0A0B0C0D;        // Value using 16 bit atomic element size

   /**
    * @return the longVal
    */
   public long getLongVal() {
      return longVal;
   }

   /**
    * @param longVal the longVal to set
    */
   public void setLongVal(long longVal) {
      this.longVal = longVal;
   }
}

@MapStructure(byteOrder = ByteOrder.LITTLE_ENDIAN)
class MyNumericLEStructure {

   @MapConstant
   @MapNumeric(type = NumericType.SIGNED_16BIT, position = 0)
   public short shortConst = Short.MAX_VALUE;

   @MapNumeric(type = NumericType.SIGNED_32BIT, position = 1)
   public short inCastVal;                                  // Cast signed 32 bit into 16 bit

   @MapNumeric(type = NumericType.SIGNED_64BIT, position = 2)
   public long longVal;

   @MapNumeric(type = NumericType.UNSIGNED_32BIT, position = 3)
   public long classicValue = 0x0A0B0C0D;

   @MapNumeric(type = NumericType.UNSIGNED_32BIT, atomicElementSize = AtomicElementSize.ATOMIC_ELEMENT_16BIT, position = 4)
   public long classicValueAtomic16 = 0x0A0B0C0D;        // Value using 16 bit atomic element size

}

@MapStructure
class MyNumericArrayStructure {

   @MapNumeric(type = NumericType.SIGNED_32BIT, position = 0)
   @MapArray(size = 3)
   private int ints[];

   /**
    * @return the ints
    */
   public int[] getInts() {
      return ints;
   }

   /**
    * @param ints the ints to set
    */
   public void setInts(int[] ints) {
      this.ints = ints;
   }
}

@MapStructure
class MyNumericMapErrorStructure {

   @MapNumeric(type = NumericType.UNSIGNED_32BIT, position = 0)
   public char charErr;
}
