/*
 * BooleanTest.java
 *
 * Defines a class used used to test memory mapper
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.memory.map;

import java.nio.ByteBuffer;
import junit.framework.*;
import org.rogueware.memory.map.annotation.MapArray;
import org.rogueware.memory.map.annotation.MapBoolean;
import org.rogueware.memory.map.annotation.MapConstant;
import org.rogueware.memory.map.annotation.MapOffset;
import org.rogueware.memory.map.annotation.MapStructure;
import org.rogueware.memory.map.enums.BooleanBit;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class BooleanTest extends TestCase {

   /**
    * Creates a new instance of XMLConfigurationTest
    *
    * @param testName
    */
   public BooleanTest(String testName) {
      super(testName);
   }

   private void checkByte(int b, int check) {
      if (b != check) {
         fail("Byte check failed. Expecting byte 0x" + Integer.toString(check, 16) + " got byte 0x" + Integer.toString(b, 16));
      }
   }

   public void testEncode() {
      MyBooleanStructure structure = new MyBooleanStructure();
      structure.xxBByte = true;
      structure.bit0 = true;
      structure.bit2 = true;
      structure.bit5 = false;
      structure.bit6 = true;
      structure.bit7 = true;
      boolean bArr[] = {true, false, true};
      structure.boolArr = bArr;

      try {
         ByteBuffer result = MemoryMap.encode(structure);

         // Check the byte buffer result to ensure encoding
         checkByte(result.get(), 0x8);  // Constant value written
         checkByte(result.get(), -0x1);  // Byte bit0 - bit7 is set for true for full byte value
         checkByte(result.get(), 0x0);  // Offset of other booleans is at pos 3
         checkByte(result.get(), -0x3b);  // -0x3b is 11000101
         checkByte(result.get(), 0x0);   // Offset of other booleans is at pos 5
         checkByte(result.get(), 0x2);   // Arr element 1 bit 1 set for true
         checkByte(result.get(), 0x0);   // Arr element 2 false
         checkByte(result.get(), 0x2);   // Arr element 3 bit 1 set for true
         checkByte(result.get(), 0x0);   // Offset of other booleans is at pos 9
         checkByte(result.get(), -0x1);   // Const Arr element 1 bit0 - bit 7 set for true
         checkByte(result.get(), -0x1);   // Const Arr element 2 bit 0 - bit 7 set for true
         checkByte(result.get(), -0x19);  // Bit increment value 11100111
         checkByte(result.get(), 0x18);   // Bit increment value 00011000

         if (result.remaining() > 0) {
            fail("Too many bytes remaining in result");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Boolean encode test failed with exception: " + ex.getMessage());
      }
   }

   public void testDecode() {
      MyBooleanStructure structure = null;

      try {
         byte srcArr[] = {0x8, Byte.parseByte("00000001", 2), 0x0, -0x3b, 0x0, 0x2, 0x0, 0x2, 0x0, 0x1, 0x1, -0x19, 0x18};
         ByteBuffer src = ByteBuffer.wrap(srcArr);

         structure = MemoryMap.decode(MyBooleanStructure.class, src);

         if (!structure.xxBByte) {
            fail("xxxBByte supposed to be true");
         }
         if (!structure.bit0) {
            fail("bit0 supposed to be true");
         }
         if (!structure.bit2) {
            fail("bit2 supposed to be true");
         }
         if (structure.bit5) {
            fail("bit5 supposed to be false");
         }
         if (!structure.bit6) {
            fail("bit6 supposed to be true");
         }
         if (!structure.bit7) {
            fail("bit7 supposed to be true");
         }
         if (!structure.boolArr[0]) {
            fail("boolArr[0] supposed to be true");
         }
         if (structure.boolArr[1]) {
            fail("boolArr[1] supposed to be false");
         }
         if (!structure.boolArr[2]) {
            fail("boolArr[2] supposed to be true");
         }
         if (!structure.boolConstArr[0]) {
            fail("boolConstArr[0] supposed to be true");
         }
         if (!structure.boolConstArr[1]) {
            fail("boolConstArr[1] supposed to be true");
         }

         boolean cmpArr[] = {true, true, true, false, false, true, true, true, false, false, false, true, true, false, false, false};
         for (int i = 0; i < 16; i++) {
            if (structure.allBitsArr[i] != cmpArr[i]) {
               fail("allBitsArr at index " + i + " did not decode");
            }
         }

         if (src.remaining() > 0) {
            fail("Too many bytes remaining in source buffer");
         }
      } catch (MemoryMapException ex) {
         ex.printStackTrace(System.err);
         fail("Boolean decode test failed with exception: " + ex.getMessage());
      }
   }

   public void testError() {
      // Error 1
      try {
         MyBooleanArrNoIncErrorStructure structure = new MyBooleanArrNoIncErrorStructure();

         ByteBuffer result = MemoryMap.encode(structure);

         fail("IncrementOffset false error not generated");
      } catch (MemoryMapException ex) {
         if (!ex.getMessage().contains("contains a boolean array with MapBoolean annotation incrementOffset set to false")) {
            fail("Expecting exception message containing 'contains a boolean array with MapBoolean annotation incrementOffset set to false'");
         }
      }

      // Error 2
      try {
         MyBooleanMapErrorStructure structure = new MyBooleanMapErrorStructure();

         ByteBuffer result = MemoryMap.encode(structure);

         fail("Type map error not generated");
      } catch (MemoryMapException ex) {
         if (!ex.getMessage().contains("MapBoolean annotation must annotate a boolean primitive field")) {
            fail("Expecting exception message containing 'MapBoolean annotation must annotate a boolean primitive field'");
         }
      }
   }
}

@MapStructure
class MyBooleanStructure {

   @MapConstant
   @MapBoolean(bit = BooleanBit.BIT_3, position = 0)
   public boolean boolConst = true;

   @MapBoolean(position = 1)
   public boolean xxBByte = false;     // Test encoding full byte

   // Byte 2=0x0
   @MapOffset(relative = 1)

   @MapBoolean(bit = BooleanBit.BIT_0, incrementOffset = false, position = 2)
   public boolean bit0 = false;
   @MapBoolean(bit = BooleanBit.BIT_2, incrementOffset = false, position = 3)
   public boolean bit2 = false;
   @MapBoolean(bit = BooleanBit.BIT_5, incrementOffset = false, position = 4)
   public boolean bit5 = true;
   @MapBoolean(bit = BooleanBit.BIT_6, incrementOffset = false, position = 5)
   public boolean bit6 = false;
   @MapBoolean(bit = BooleanBit.BIT_7, position = 6)   // Move to next byte
   public boolean bit7 = false;

   // Byte 4=0x0
   @MapOffset(relative = 1)

   @MapArray(size = 3)
   @MapBoolean(bit = BooleanBit.BIT_1, position = 7)
   public boolean boolArr[];

   // Byte 8=0x0
   @MapOffset(relative = 1)

   @MapConstant
   @MapArray(size = 2)
   @MapBoolean(position = 8)
   public boolean boolConstArr[] = {true, true};

   @MapArray(size = 16)
   @MapBoolean(bit = BooleanBit.BIT_ALL, position = 9)
   public boolean allBitsArr[] = {true, true, true, false, false, true, true, true, false, false, false, true, true, false, false, false};
}

// Errors
@MapStructure
class MyBooleanArrNoIncErrorStructure {

   // Cannot set increment = false for array mapping of boolean.
   // Arrray mappings are limited to 1 byte at a time
   @MapArray(size = 3)
   @MapBoolean(bit = BooleanBit.BIT_1, incrementOffset = false, position = 0)
   public boolean boolArr[];
}

@MapStructure
class MyBooleanMapErrorStructure {

   // Cannot set increment = false for array mapping of boolean.
   // Arrray mappings are limited to 1 byte at a time
   @MapBoolean(position = 0)
   public int boolErr;
}
