# Memory Map API


## Overview

Memory Map API is a Java utility library used to simplify the encoding and decoding of binary or text based raw data between POJO's annotated with defined annotations governing the encoding and decoding rules.

The Memory MAP API annotations allow for specifying Endianness or byte order for numeric data mapping, the handling of signed and unsigned value mapping between signed Java primitives, known date conversions, various string conversions such as null terminated C strings using encodings such as ASCII and Unicode and mapping of arrays of primitives or further annotated POJO's. 

Annotated constants can be used to distinguish magics for decoding one or many annotated POJO's from a single data stream.